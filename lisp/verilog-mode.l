;;; -*- Mode: Lisp; Package: EDITOR -*-
;;;
;;; This file is part of xyzzy.
;;;

(provide "verilog-mode")

(in-package "editor")

(export '(verilog-mode
					decode-c-mode
					verilog-indent-level
					verilog-continued-statement-offset
					verilog-argdecl-indent
					verilog-brace-offset
					verilog-brace-imaginary-offset
					verilog-label-offset
					verilog-comment-indent
					*default-c-mode*
					*verilog-comment-column*
					*verilog-mode-hook*
					*verilog-keyword-file*
					*verilog-indent-tabs-mode*
					)
				)

(defvar *verilog-mode-hook* nil)
(defvar *verilog-tab-always-indent* t)
(defvar *verilog-keyword-hash-table* nil)
(defvar *verilog-keyword-file* "verilog")
(defvar *verilog-indent-tabs-mode* nil)
(defvar *verilog-comment-column* nil)

(unless (boundp 'verilog-indent-level)
  (setq verilog-indent-level 2)
  (setq verilog-continued-statement-offset 2)
  (setq verilog-argdecl-indent 5)
  (setq verilog-brace-offset 0)
  (setq verilog-brace-imaginary-offset 0)
  (setq verilog-label-offset -2)
  (setq verilog-comment-indent 2)
	)


(defvar *verilog-mode-syntax-table* nil)
(unless *verilog-mode-syntax-table*
  (setq *verilog-mode-syntax-table* (make-syntax-table))
  (do ((x #x21 (1+ x)))((>= x #x7f))
    (let ((c (code-char x)))
      (unless (alphanumericp c)
	(set-syntax-punctuation *verilog-mode-syntax-table* c))))
  (set-syntax-option *verilog-mode-syntax-table*
		     (+ *syntax-option-c-preprocessor*
			*syntax-option-indent-c++*))
  (set-syntax-string *verilog-mode-syntax-table* #\")
  (set-syntax-escape *verilog-mode-syntax-table* #\\)
  (set-syntax-symbol *verilog-mode-syntax-table* #\_)
  (set-syntax-symbol *verilog-mode-syntax-table* #\#)
  (set-syntax-symbol *verilog-mode-syntax-table* #\`)
  (set-syntax-match *verilog-mode-syntax-table* #\( #\))
  (set-syntax-match *verilog-mode-syntax-table* #\{ #\})
  (set-syntax-match *verilog-mode-syntax-table* #\[ #\])
  (set-syntax-start-multi-comment *verilog-mode-syntax-table* "/*")
  (set-syntax-end-multi-comment *verilog-mode-syntax-table* "*/")
  (set-syntax-start-c++-comment *verilog-mode-syntax-table* #\/)
  (set-syntax-end-c++-comment *verilog-mode-syntax-table* #\LFD)
	)

(defvar *verilog-mode-map* nil)
(unless *verilog-mode-map*
  (setq *verilog-mode-map* (make-sparse-keymap))
  (define-key *verilog-mode-map* #\{ 'c-electric-insert)
  (define-key *verilog-mode-map* #\: 'c-electric-insert)
  (define-key *verilog-mode-map* #\# 'c-electric-insert)
  (define-key *verilog-mode-map* #\} 'c-electric-close)
  (define-key *verilog-mode-map* #\C-h 'backward-delete-char-untabify-or-selection)
  (define-key *verilog-mode-map* #\TAB 'c-indent-line)
  (define-key *verilog-mode-map* #\C-M-q 'indent-sexp)
  (define-key *verilog-mode-map* #\RET 'c-newline-and-indent)
	)

(defvar *verilog-mode-abbrev-table* nil)
(unless *verilog-mode-abbrev-table*
  (define-abbrev-table '*verilog-mode-abbrev-table*)
	)

(autoload 'c-build-summary-of-functions "cfns" nil)

(defun verilog-mode ()
  (interactive)
  (kill-all-local-variables)
  (setq mode-name "verilog")
  (setq buffer-mode 'verilog-mode)
  (use-syntax-table *verilog-mode-syntax-table*)
  (use-keymap *verilog-mode-map*)
  (make-local-variable 'mode-specific-indent-command)
  (setq mode-specific-indent-command 'c-indent-line)
  (make-local-variable 'c-comment-indent-variable)
  (setq c-comment-indent-variable 'verilog-comment-indent)
  (make-local-variable 'paragraph-start)
  (setq paragraph-start "^$\\|\f")
  (make-local-variable 'paragraph-separate)
  (setq paragraph-separate paragraph-start)
  (make-local-variable 'indent-tabs-mode)
  (setq indent-tabs-mode *verilog-indent-tabs-mode*)
  (make-local-variable 'tags-find-target)
  (setq tags-find-target #'c-tags-find-target)
  (make-local-variable 'tags-find-point)
  (setq tags-find-point #'c-tags-find-point)
  (make-local-variable 'build-summary-function)
  (setq build-summary-function 'c-build-summary-of-functions)
  (and *verilog-keyword-file*
			 (null *verilog-keyword-hash-table*)
			 (setq *verilog-keyword-hash-table*
				 (load-keyword-file *verilog-keyword-file*)))
	(when *verilog-keyword-hash-table*
    (make-local-variable 'keyword-hash-table)
    (setq keyword-hash-table *verilog-keyword-hash-table*))
  (setq *local-abbrev-table* *verilog-mode-abbrev-table*)
  (setq comment-start "// ")
  (setq comment-end "")
  (setq comment-start-skip "/\\(\\*+\\|/\\)[ \t]*")
  (setq comment-indent-function 'c-comment-indent)
  (when *verilog-comment-column*
    (setq comment-column *verilog-comment-column*))
;;;===========================================================================
  (extend-verilog-coloring-by-regexp)
;;;===========================================================================
  (run-hooks '*verilog-mode-hook*))

(defvar *default-c-mode* 'c-mode)

(setf (get 'decode-c-mode 'decode-auto-mode) 't)

(defun decode-c-mode (&optional last-buffer)
  (interactive)
  (let (mode)
    (save-excursion
      (goto-char (point-min))
      (if (or (scan-buffer "//" :limit 3000)
	      (scan-buffer "\\(^\\|[^A-Za-z0-9_]\\)class\\($\\|[^A-Za-z0-9_]\\)"
			   :regexp t :limit 3000)
	      (scan-buffer "\\(^\\|[^A-Za-z0-9_]\\)\\(public\\|private\\|protected\\)[ \t\n\f]*:"
			   :regexp t :limit 3000))
	  (setq mode 'verilog-mode)
	(when last-buffer
	  (set-buffer last-buffer)
	  (if (and (boundp 'buffer-mode)
		   (or (eq buffer-mode 'c-mode)
		       (eq buffer-mode 'verilog-mode)))
	      (setq mode buffer-mode)))))
    (funcall (or mode *default-c-mode*))))


;;;===========================================================================
(defun extend-verilog-coloring-by-regexp ()
  (interactive)
  (defvar *c-label-formats*
    (compile-regexp-keyword-list
     '(
       ("\\_<\\([1-9][0-9]*\\)?'[bodh][0-9a-zA-Z_XxZz]+\\_>" t :tag)
       ("[-][>]" t (:color 2 0 :bold))	;; �wevent �g���K�x
       ("[$][0-9a-zA-Z_]+" t (:keyword 1))
       )))
  (make-local-variable 'regexp-keyword-list)
  (setq regexp-keyword-list *c-label-formats*))
;;;===========================================================================
