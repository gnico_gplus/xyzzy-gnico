;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; 
;;;    hs-mode 
;;;
;;;    a simple haskell mode for xyzzy
;;;
;;;    by T.Shido
;;;    http:http://www.geocities.jp/shido_takafumi/hs-mode.html
;;;    shido_takafumi@ybb.ne.jp
;;;
;;;    March 15, 2005 Var 0.1.2 debug 
;;;    March 12, 2005 Var 0.1.1 bug fix
;;;    March 04, 2005 Var 0.1 
;;;
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Following interactive functions are available:
;;; 
;;; function name: hs-indent-line
;;; default key: #\TAB
;;; note: indent a line according to the layout criteria of Haskell
;;; 
;;; function name: hs-newline-and-indent
;;; default key: #\RET
;;; note: insert return and indent current line.
;;; 
;;; function name: hs-completion
;;; default key: #\C-.
;;; note: complete words
;;; 
;;; function name: hs-unindent-line
;;; default key: #\S-TAB
;;; note: reduce the indent depth
;;; 
;;; function name: hs-more-indent
;;; default key: #\S-SPC
;;; note: insert *hs-primary-indent* spaces
;;; 
;;; function name: hs-insert-guard
;;; default key: #\M-g
;;; note: insert a guard ("| ") with indent
;;; 
;;; function name: hs-insert-where
;;; default key: #\M-w
;;; note: insert "where " with indent
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide "hs-mode")
(in-package "editor")

(export '(hs-mode
          *hs-keyword-file* *hs-mode-hook*
          *hs-mode-syntax-table* *hs-mode-map*
          *hs-primary-indent* *hs-string-indent* *hs-comment-indent*
          *hs-secondary-indent* *hs-mode-abbrev-table* ))

(defvar *hs-mode-hook* nil)
(defvar *hs-keyword-hash-table* nil)
(defvar *hs-keyword-file* "Haskell")
(defvar *hs-completion-list* nil)

(defvar *hs-primary-indent* 4)
(defvar *hs-string-indent* 0)
(defvar *hs-comment-indent* 2)
(defvar *hs-secondary-indent* 1)

(defvar *hs-mode-abbrev-table* nil)
(unless *hs-mode-abbrev-table*
  (define-abbrev-table '*hs-mode-abbrev-table*))

(defvar *hs-mode-syntax-table* nil)
(unless *hs-mode-syntax-table*
  (setq *hs-mode-syntax-table* (make-syntax-table))
  (set-syntax-string *hs-mode-syntax-table* #\")
  (set-syntax-escape *hs-mode-syntax-table* #\\)
  (set-syntax-escape *hs-mode-syntax-table* #\')
  (set-syntax-symbol *hs-mode-syntax-table* #\!)
  (set-syntax-symbol *hs-mode-syntax-table* #\=)
  (set-syntax-symbol *hs-mode-syntax-table* #\:)
  (set-syntax-symbol *hs-mode-syntax-table* #\|)
  (set-syntax-symbol *hs-mode-syntax-table* #\.)
  (set-syntax-symbol *hs-mode-syntax-table* #\;)
  (set-syntax-symbol *hs-mode-syntax-table* #\`)
  (set-syntax-match *hs-mode-syntax-table* #\( #\))
  (set-syntax-match *hs-mode-syntax-table* #\[ #\])
  (set-syntax-match *hs-mode-syntax-table* #\{ #\})
  (set-syntax-start-multi-comment *hs-mode-syntax-table* "{-")
  (set-syntax-end-multi-comment *hs-mode-syntax-table* "-}")
  (set-syntax-start-c++-comment *hs-mode-syntax-table* #\-)
  (set-syntax-end-c++-comment *hs-mode-syntax-table* #\LFD)
  (set-syntax-word *hs-mode-syntax-table* #\_)
  (set-syntax-word *hs-mode-syntax-table* #\'))

(set-extended-key-translate-table exkey-S-tab #\F20)
(set-extended-key-translate-table exkey-S-space #\F21)
(defvar *hs-mode-map* nil)
(unless *hs-mode-map*
  (setq *hs-mode-map* (make-sparse-keymap))
  (define-key *hs-mode-map* #\] 'hs-electric-close)
  (define-key *hs-mode-map* #\} 'hs-electric-close)
  (define-key *hs-mode-map* #\) 'hs-electric-close)
  (define-key *hs-mode-map* #\( 'hs-electric-open)
  (define-key *hs-mode-map* #\TAB 'hs-indent-line)
  (define-key *hs-mode-map* #\F20 'hs-unindent-line)
  (define-key *hs-mode-map* #\F21 'hs-more-indent)
  (define-key *hs-mode-map* #\C-. 'hs-completion)
  (define-key *hs-mode-map* #\RET 'hs-newline-and-indent)
  (define-key *hs-mode-map* #\M-g 'hs-insert-guard)
  (define-key *hs-mode-map* #\M-w 'hs-insert-where))

;;;;; macros
(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form))
     (if it ,then-form ,else-form)))

(defmacro in (test target &rest candidates)
  (let ((gtarget (gensym)))
    `(let ((,gtarget ,target))
       (or
        ,@(mapcar #'(lambda (x) `(,test ,gtarget ,x)) candidates)))))

;;;;;;functions
(defun hs-max (&rest rest)
  (let ((ls (remove nil rest)))
    (cond
     ((null ls) nil)
     ((null (cdr ls)) (car ls))
     (t (apply 'max ls)))))

(defun hs-goto-eol ()
  (goto-eol)
  (while (parse-point-syntax)
    (backward-char))
  (skip-chars-backward " \t"))

(defun hs-eol-with-op ()
  (save-excursion
    (hs-goto-eol)
    (in char= (preceding-char) #\+ #\- #\*  #\/ #\% #\` #\> #\< #\| #\& #\^ #\!)))

;;; check if the current line is comment or string
(defun hs-comment-line-p ()
  (save-excursion
    (or
     (parse-point-syntax)
     (let ((c1 (following-char)))
       (when (or (syntax-c++-comment-p c1) (syntax-start-multi-comment-1-p c1))
         (forward-char)
         (let ((c2 (following-char)))
           (or (syntax-c++-comment-p c2) (syntax-start-multi-comment-1-p c2))))))))

(defun hs-get-indent (&optional start)
  (save-excursion
    (if start
        (goto-char start)
      (goto-bol))
    (skip-chars-forward " \t")
    (current-column)))

;;; check if the position is in parentheses
(defun hs-in-parenthes-p ()
  (save-excursion
    (let ((p0 (point)))
      (and
       (hs-scan-buffer "[" :reverse t)
       (not (hs-scan-buffer "]" :limit p0))))))

;;; forward-line with skipping comments and string
(defun hs-forward-line (&optional (n 1))
  (let ((i 0))
    (if (> n 0)
        (while (< i n)
          (or (forward-line) (return-from hs-forward-line nil))
          (or (hs-comment-line-p)  (incf i)))
      (while (> i n)
        (or (forward-line -1) (return-from hs-forward-line nil))
        (or (hs-comment-line-p) (decf i)))))
  t)

;;; scan-buffer with skipping comments and string
(defun hs-scan-buffer (str &key tail case-fold reverse word-search limit regexp)
  (while (scan-buffer str :no-dup t :case-fold case-fold :reverse reverse
                      :word-search word-search :limit limit
                      :regexp regexp :tail tail)
    (or (parse-point-syntax) (return-from hs-scan-buffer t))))

;;; return the position of unclosed 
(defun hs-find-open (start limit)
  (goto-char start)
  (let (acc)
    (while (< (point) limit)
      (unless (parse-point-syntax)
        (let ((c (following-char)))
          (if (syntax-open-p c) (push (point) acc))
          (if (syntax-close-p c) (pop acc))))
      (forward-char))
    (if acc (+ 2 (- (car acc) start)))))

(defun hs-pair-char (c)
  (case c
    (#\] "[")
    (#\) "(")
    (#\} "{")
    (t "")))
 
(defun hs-find-close (start limit)
  (goto-char limit)
  (if (and (= (current-column) 1) (in char= (preceding-char) #\] #\} #\)))
      0)
  (while (hs-scan-buffer "[\]\)\}]" :regexp t :reverse t :limit start)
    (let ((tstr (hs-pair-char (char (match-string 0) 0)))
          (p (match-beginning 0)))
      (or (hs-scan-buffer tstr :limit start :reverse t)
          (return-from hs-find-close
            (progn
              (goto-char p)
              (goto-matched-parenthesis)
              (hs-calc-indent-aux)))))))

(defun hs-find-next-to-eq (start limit)
  (goto-char limit)
  (unless (hs-eol-with-op)
    (hs-forward-line -1)
    (when (hs-eol-with-op)
      (hs-forward-line 1)
      (hs-get-previous-indent))))

;;; indent after "if"
(defun hs-find-if (start limit)
  (goto-char start)
  (if (scan-buffer "\\(^\\|[ \t]\\)\\(if\\)\\($\\|[ \t]\\)" :regexp t :limit limit)
      (let ((p (match-beginning 2)))
        (unless (hs-scan-buffer "else" :limit limit :word-search t)
          (+ (- p start) *hs-primary-indent*)))))

;;; indent after ".... ="
(defun hs-find-equal (start)
  (goto-char start)
  (if (scan-buffer " =[ \t]*\\(\\(--\\|\{-\\).*\\)?$"
                   :limit (prog2 (goto-eol) (point) (goto-bol))
                   :regexp t)
   (+ (hs-get-indent) *hs-primary-indent*)))

;;;indent after guard (| ..... = ......)        
(defun hs-find-guard (start limit)
  (goto-char start)
  (while (scan-buffer " \| .* = " :limit limit :no-dup t :regexp t)
    (let ((p (1+ (match-beginning 0))))
      (or (parse-point-syntax)
          (hs-in-parenthes-p)
          (return-from hs-find-guard
            (if (progn
                  (goto-char (+ 2 p))
                  (skip-chars-forward " \t")
                  (looking-for "otherwise "))
                (hs-get-previous-indent)
              (- p start)))))))

;;;indent after keywords (do, let, of, where)
(defun hs-find-keywords (start limit &optional colmax)
  (labels ((rec (col id)
             (if (scan-buffer "\\(^\\| \\)\\(do\\|let\\|of\\|where\\|bracket\\)\\($\\|[ \t]+\\)"
                              :limit limit :regexp t)
                 (let ((str (match-string 2))
                       (end (match-end 0)))
                   (let ((col2
                          (cond
                           ((progn (goto-char end) (eolp))
                            (+ id *hs-primary-indent*))
                           ((and (string= str "let")
                                 (scan-buffer "in" :word-search t :limit limit))
                            id)
                           (t (+ (progn (goto-char end) (current-column))
                                 (if (hs-find-equal start) *hs-primary-indent* 0))))))
                     (goto-char (1- end))
                     (if (and colmax (>= col2 colmax))
                         col
                       (rec col2 id))))
               col)))
    (goto-char start)
    (rec nil (hs-get-indent)))) 

;;; indent after data
(defun hs-find-data (start limit)
  (goto-char start)
  (if (looking-for "data ")
      (if (hs-scan-buffer " = " :limit limit)
          (- (match-end 0) start 2)
        *hs-primary-indent*)))

;;; class or instance
(defun hs-find-class (start)
  (goto-char start)
  (if (or (looking-for "instance ") (looking-for "class "))
      *hs-primary-indent*))

;;; no indent
(defun hs-no-indent (start limit)
  (goto-char start)
  (if (or (looking-for "module")
          (hs-empty-line-p))
      0))
  
;;; indent after continuing equation
(defun hs-equation-continue (start limit)
  (goto-char limit)
  (when (hs-eol-with-op)
    (or (hs-scan-buffer "\\( =\\)\\|\\( ::\\)" :reverse t :limit start :tail t :regexp t)
        (goto-bol))
    (skip-chars-forward " \t")
    (- (point) start)))

;;; calculating the indent of current line
(defun hs-calc-indent ()
  (save-excursion
    (if (hs-forward-line -1)
        (hs-calc-indent-aux)
      0)))

(defun hs-calc-indent-aux ()
  (let ((start (progn (goto-bol) (point)))
        (limit (progn (hs-goto-eol) (point))))
    (or
     (hs-no-indent start limit)
     (hs-find-data start limit)
     (hs-find-class start)
     (hs-find-if start limit)
     (hs-max
      (hs-equation-continue start limit)
      (hs-find-keywords start limit)
      (hs-find-open start limit)
      (hs-find-guard start limit)
      (hs-find-equal start))
     (hs-find-close start limit)
     (hs-find-next-to-eq start limit)
     (hs-get-indent start))))

;;; calculation indent for where
(defun hs-where-indent ()
  (save-excursion
    (while (hs-forward-line -1)
      (or (hs-isguard t) (return)))
    (hs-calc-indent-aux)))

(defun hs-guard-indent ()
  (save-excursion
    (hs-forward-line -1)
    (aif (hs-isguard)
        it
      (+ (hs-get-indent) *hs-primary-indent*))))
     
;;;
(defun hs-isin ()
  (save-excursion
    (goto-bol)
    (skip-chars-forward " \t")
    (looking-at "in\\($\\|[ \t]\\)")))

(defun hs-inindent ()
  (save-excursion
    (hs-scan-buffer "let" :reverse t :word-search t)
    (current-column)))3

(defun hs-iswhere ()
  (save-excursion
    (goto-bol)
    (skip-chars-forward " \t")
    (when (looking-for "where")
      (forward-char 5)
      (or (eolp) (let ((c (following-char)))
                   (or (char= c #\SPC) (char= c #\TAB)))))))

(defun hs-isguard (&optional only-addtional)
  (save-excursion
    (if only-addtional
        (progn
          (goto-bol)
          (skip-chars-forward " \t")
          (looking-at "\| .* = "))
      (hs-find-guard
       (progn (goto-bol) (point))
       (progn (hs-goto-eol) (point))))))

;;; indent this line
(defun hs-indent-line ()
  (interactive)
  (or (parse-point-syntax)
      (let ((id (cond
                 ((hs-isin) (hs-inindent))
                 ((hs-iswhere) (+ *hs-secondary-indent* (hs-where-indent)))
                 ((hs-isguard t) (hs-guard-indent))
                 (t (hs-calc-indent)))))
        (goto-bol)
        (looking-at "[ \t]*")
        (delete-region (match-beginning 0) (match-end 0))
        (indent-to id))))

;;; unindent this line
(defun hs-unindent-line ()
  (interactive)
  (let ((iswhere (hs-iswhere)))
    (aif (hs-get-previous-indent iswhere)
        (progn
          (delete-region
           (progn (goto-bol) (point))
           (progn (skip-chars-forward " \t") (point)))
          (indent-to (+ it (if iswhere
                               *hs-secondary-indent* 0)))))))

(defun hs-get-previous-indent (&optional iswhere)
  (save-excursion
    (let ((id0 (- (hs-get-indent) (if iswhere *hs-secondary-indent* 0 ))))
      (if (< 0 id0)
        (while (hs-forward-line -1)
          (aif (hs-tab id0)
              (return-from hs-get-previous-indent it)))))))

(defun hs-tab (id0)
  (let ((start (progn (goto-bol) (point)))
        (limit (progn (hs-goto-eol) (point))))
    (let ((id1 (hs-get-indent start)))
      (when (< id1 id0)
        (or
         (hs-find-max id0
                      (hs-find-data start limit)
                      (hs-find-class start)
                      (hs-find-if start limit)
                      (hs-equation-continue start limit)
                      (hs-find-keywords start limit id0)
                      (hs-find-open start limit)
                      (hs-find-guard start limit)
                      (hs-find-equal start)
                      (hs-find-close start limit)
                      (hs-find-next-to-eq start limit))
         (- id1 (if (progn (goto-char start) (hs-iswhere))
                    *hs-secondary-indent* 0)))))))

(defun hs-find-max (limit &rest tabls)
  (labels ((rec (tabmax ls)
             (if ls
                 (let ((c (car ls)))
                   (rec
                    (if (and c (< c limit) (< tabmax c)) c tabmax)
                    (cdr ls)))
               (if (< 0 tabmax) tabmax))))
    (rec 0 tabls)))

;;; newline and indent
(defun hs-newline-and-indent ()
  (interactive)
  (delete-trailing-spaces)
  (when (hs-isin)
    (goto-bol)
    (looking-at "[ \t]*")
    (delete-region (match-beginning 0) (match-end 0))
    (indent-to (hs-inindent))
    (goto-eol))
  (insert #\LFD)
  (indent-to
   (case (parse-point-syntax)
     (:string *hs-string-indent*)
     (:comment *hs-comment-indent*)
     (t (hs-calc-indent)))))

;;; inserting guard with indent
(defun hs-insert-guard ()
  (interactive)
  (indent-to (hs-guard-indent))
  (insert "| "))

(defun hs-empty-line-p ()
  (save-excursion
    (goto-bol)
    (skip-chars-forward " \t")
    (or (eolp)
        (progn
          (forward-char)
          (eq (parse-point-syntax) ':comment)))))

;;; inserting where with indent
(defun hs-insert-where ()
  (interactive)
  (when (hs-empty-line-p)
    (delete-region
     (progn (goto-bol) (point))
     (progn (hs-goto-eol) (point)))
    (indent-to (+ *hs-secondary-indent* (hs-where-indent))))
  (insert "where "))

;;; more indent
(defun hs-more-indent ()
  (interactive)
  (let ((p (point)))
    (goto-bol)
    (insert " " *hs-primary-indent*)
    (goto-char (+ p *hs-primary-indent*))))
  
(defun hs-electric-close (&optional (arg 1))
  (interactive "*p")
  (unless (prog1 (parse-point-syntax)
            (self-insert-command arg))
    (let ((p (1- (point))))
      (goto-bol)
      (skip-chars-forward " \t")
      (if (looking-at "[\]\}\)]$")
          (progn
            (delete-region (point) (progn (goto-bol) (point)))
            (indent-to (save-excursion
                         (progn
                           (goto-matched-parenthesis)
                           (current-column)))))
        (goto-char p))
      (when (goto-matched-parenthesis)
        (show-matched-parenthesis)
        (goto-matched-parenthesis))
      (forward-char))))

(defun hs-electric-open (&optional (arg 1))
  (interactive "*p")
  (unless (prog1 (parse-point-syntax)
            (self-insert-command arg))
    (backward-char arg)
    (if (char= (preceding-char) #\\)
        (insert #\SPC))
    (forward-char arg)))

;;; word completion
(defun hs-completion ()
  (interactive)
  (or *hs-completion-list*
      (setq *hs-completion-list* (make-list-from-keyword-table *hs-keyword-hash-table*))
      (return-from hs-completion nil))
  (let ((opoint (point)))
    (when (skip-syntax-spec-backward "w_")
      (let ((from (point)))
        (goto-char opoint)
        (do-completion from opoint :list *hs-completion-list*)))))

;;; haskell mode
(defun hs-mode ()
  (interactive)
  (kill-all-local-variables)
  (setq mode-name "Haskell")
  (setq buffer-mode 'hs-mode)
  (use-syntax-table *hs-mode-syntax-table*)
  (use-keymap *hs-mode-map*)
  (make-local-variable 'mode-specific-indent-command)
  (setq mode-specific-indent-command #'hs-indent-line)
    (make-local-variable 'indent-tabs-mode)
  (setq indent-tabs-mode nil)
    (and *hs-keyword-file*
         (null *hs-keyword-hash-table*)
         (setq *hs-keyword-hash-table*
               (load-keyword-file *hs-keyword-file*)))
  (when *hs-keyword-hash-table*
    (make-local-variable 'keyword-hash-table)
    (setq keyword-hash-table *hs-keyword-hash-table*))
  (make-local-variable 'regexp-keyword-list)
  (setq *local-abbrev-table* *hs-mode-abbrev-table*)
  (run-hooks '*hs-mode-hook*))

