#!/bin/sh

BINDIR=../bin

while :; do
  case "$1" in
  -d)
    BINDIR="$2"
    shift
    shift
    ;;
  *)
    break
    ;;
  esac
done

if [ $# = 0 ]; then
  echo "usage: $0 [-d bindir] dicfile" 2>&1
  exit 2
fi

PATH="$BINDIR:$PATH" export PATH
DICT="$1"

munchlist -v -l english.aff "$DICT" english.0 english.1 american.0 american.1 > american.med+
if [ -s american.med+ ]; then
  rm -f americanmed+.hash
  buildhash american.med+ english.aff americanmed+.hash
else
  echo "error: zero-length dictionary generated"
  rm -f american.med+
  exit 2
fi
