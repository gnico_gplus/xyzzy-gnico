static inline float
coerce_to_single_floats (long x, lisp lx)
//#line 5 "../src/num-arith.d"
{return float (x);}

static inline float
coerce_to_single_floatl (long x, lisp lx)
//#line 6 "../src/num-arith.d"
{return float (x);}

static inline float
coerce_to_single_floatb (const bignum_rep *x, lisp lx)
//#line 7 "../src/num-arith.d"
{return float (x->to_double ());}

static inline float
coerce_to_single_floatr (const lfraction *x, lisp lx)
//#line 8 "../src/num-arith.d"
{return fract_to_single_float (x);}

static inline float
coerce_to_single_floatF (float x, lisp lx)
//#line 9 "../src/num-arith.d"
{return x;}

static inline float
coerce_to_single_floatD (double x, lisp lx)
//#line 10 "../src/num-arith.d"
{return float (x);}

//#line 2 "../src/num-arith.d"
float
coerce_to_single_float (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return coerce_to_single_floats (xshort_int_value (x), x);
    case Tlong_int: return coerce_to_single_floatl (xlong_int_value (x), x);
    case Tbignum: return coerce_to_single_floatb (xbignum_rep (x), x);
    case Tfraction: return coerce_to_single_floatr ((lfraction *) (x), x);
    case Tsingle_float: return coerce_to_single_floatF (xsingle_float_value (x), x);
    case Tdouble_float: return coerce_to_single_floatD (xdouble_float_value (x), x);
    }
}

static inline double
coerce_to_double_floats (long x, lisp lx)
//#line 16 "../src/num-arith.d"
{return x;}

static inline double
coerce_to_double_floatl (long x, lisp lx)
//#line 17 "../src/num-arith.d"
{return x;}

static inline double
coerce_to_double_floatb (const bignum_rep *x, lisp lx)
//#line 18 "../src/num-arith.d"
{return x->to_double ();}

static inline double
coerce_to_double_floatr (const lfraction *x, lisp lx)
//#line 19 "../src/num-arith.d"
{return fract_to_double_float (x);}

static inline double
coerce_to_double_floatF (float x, lisp lx)
//#line 20 "../src/num-arith.d"
{return x;}

static inline double
coerce_to_double_floatD (double x, lisp lx)
//#line 21 "../src/num-arith.d"
{return x;}

//#line 13 "../src/num-arith.d"
double
coerce_to_double_float (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return coerce_to_double_floats (xshort_int_value (x), x);
    case Tlong_int: return coerce_to_double_floatl (xlong_int_value (x), x);
    case Tbignum: return coerce_to_double_floatb (xbignum_rep (x), x);
    case Tfraction: return coerce_to_double_floatr ((lfraction *) (x), x);
    case Tsingle_float: return coerce_to_double_floatF (xsingle_float_value (x), x);
    case Tdouble_float: return coerce_to_double_floatD (xdouble_float_value (x), x);
    }
}

static inline long
coerce_to_longs (long x, lisp lx)
//#line 27 "../src/num-arith.d"
{return x;}

static inline long
coerce_to_longl (long x, lisp lx)
//#line 28 "../src/num-arith.d"
{return x;}

static inline long
coerce_to_longb (const bignum_rep *x, lisp lx)
//#line 29 "../src/num-arith.d"
{return x->coerce_to_long ();}

static inline long
coerce_to_longr (const lfraction *x, lisp lx)
//#line 30 "../src/num-arith.d"
{return long (fract_to_double_float (x));}

static inline long
coerce_to_longF (float x, lisp lx)
//#line 31 "../src/num-arith.d"
{return long (x);}

static inline long
coerce_to_longD (double x, lisp lx)
//#line 32 "../src/num-arith.d"
{return long (x);}

//#line 24 "../src/num-arith.d"
long
coerce_to_long (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return coerce_to_longs (xshort_int_value (x), x);
    case Tlong_int: return coerce_to_longl (xlong_int_value (x), x);
    case Tbignum: return coerce_to_longb (xbignum_rep (x), x);
    case Tfraction: return coerce_to_longr ((lfraction *) (x), x);
    case Tsingle_float: return coerce_to_longF (xsingle_float_value (x), x);
    case Tdouble_float: return coerce_to_longD (xdouble_float_value (x), x);
    }
}

static inline int64_t
coerce_to_int64s (long x, lisp lx)
//#line 38 "../src/num-arith.d"
{return x;}

static inline int64_t
coerce_to_int64l (long x, lisp lx)
//#line 39 "../src/num-arith.d"
{return x;}

static inline int64_t
coerce_to_int64b (const bignum_rep *x, lisp lx)
//#line 40 "../src/num-arith.d"
{return x->coerce_to_int64 ();}

static inline int64_t
coerce_to_int64r (const lfraction *x, lisp lx)
//#line 41 "../src/num-arith.d"
{return int64_t (fract_to_double_float (x));}

static inline int64_t
coerce_to_int64F (float x, lisp lx)
//#line 42 "../src/num-arith.d"
{return int64_t (x);}

static inline int64_t
coerce_to_int64D (double x, lisp lx)
//#line 43 "../src/num-arith.d"
{return int64_t (x);}

//#line 35 "../src/num-arith.d"
int64_t
coerce_to_int64 (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return coerce_to_int64s (xshort_int_value (x), x);
    case Tlong_int: return coerce_to_int64l (xlong_int_value (x), x);
    case Tbignum: return coerce_to_int64b (xbignum_rep (x), x);
    case Tfraction: return coerce_to_int64r ((lfraction *) (x), x);
    case Tsingle_float: return coerce_to_int64F (xsingle_float_value (x), x);
    case Tdouble_float: return coerce_to_int64D (xdouble_float_value (x), x);
    }
}


//#line 46 "../src/num-arith.d"
#define PBIGNUM_REP bignum_rep *
static inline PBIGNUM_REP
coerce_to_bignum_reps (long x, lisp lx,bignum_rep_long *rl)
//#line 50 "../src/num-arith.d"
{
  rl->init (x);
  return rl;
}

static inline PBIGNUM_REP
coerce_to_bignum_repb (const bignum_rep *x, lisp lx,bignum_rep_long *rl)
//#line 55 "../src/num-arith.d"
{return (bignum_rep *)x;}

//#line 47 "../src/num-arith.d"
PBIGNUM_REP
coerce_to_bignum_rep (lisp x,bignum_rep_long *rl)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP: return coerce_to_bignum_reps (xshort_int_value (x), x,rl);
    case Tlong_int: return coerce_to_bignum_reps (xlong_int_value (x), x,rl);
    case Tbignum: return coerce_to_bignum_repb (xbignum_rep (x), x,rl);
    }
}


//#line 57 "../src/num-arith.d"
#undef PBIGNUM_REP

//#line 59 "../src/num-arith.d"
static lisp cast_to_double_float (lisp);
static inline lisp
cast_to_double_floats (long x, lisp lx)
//#line 64 "../src/num-arith.d"
{return make_double_float (x);}

static inline lisp
cast_to_double_floatl (long x, lisp lx)
//#line 65 "../src/num-arith.d"
{return make_double_float (x);}

static inline lisp
cast_to_double_floatb (const bignum_rep *x, lisp lx)
//#line 66 "../src/num-arith.d"
{return make_double_float (x->to_double ());}

static inline lisp
cast_to_double_floatr (const lfraction *x, lisp lx)
//#line 67 "../src/num-arith.d"
{return make_double_float (fract_to_double_float (x));}

static inline lisp
cast_to_double_floatF (float x, lisp lx)
//#line 68 "../src/num-arith.d"
{return make_double_float (x);}

static inline lisp
cast_to_double_floatD (double x, lisp lx)
//#line 69 "../src/num-arith.d"
{return lx;}

static inline lisp
cast_to_double_floatc (const lcomplex *x, lisp lx)
//#line 70 "../src/num-arith.d"
{
  if (number_typeof (x->real) == Tdouble_float)
    return lx;
  return make_complex (cast_to_double_float (x->real),
                       cast_to_double_float (x->imag));
}

//#line 61 "../src/num-arith.d"
lisp
cast_to_double_float (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return cast_to_double_floats (xshort_int_value (x), x);
    case Tlong_int: return cast_to_double_floatl (xlong_int_value (x), x);
    case Tbignum: return cast_to_double_floatb (xbignum_rep (x), x);
    case Tfraction: return cast_to_double_floatr ((lfraction *) (x), x);
    case Tsingle_float: return cast_to_double_floatF (xsingle_float_value (x), x);
    case Tdouble_float: return cast_to_double_floatD (xdouble_float_value (x), x);
    case Tcomplex: return cast_to_double_floatc ((lcomplex *) (x), x);
    }
}


//#line 78 "../src/num-arith.d"
// 12.2
static inline lisp
Fzerops (long x, lisp lx)
//#line 83 "../src/num-arith.d"
{return boole (!x);}

static inline lisp
Fzeropb (const bignum_rep *x, lisp lx)
//#line 85 "../src/num-arith.d"
{return boole (x->zerop ());}

static inline lisp
Fzeropr (const lfraction *x, lisp lx)
//#line 86 "../src/num-arith.d"
{return Qnil;}

static inline lisp
FzeropF (float x, lisp lx)
//#line 87 "../src/num-arith.d"
{return boole (!x);}

static inline lisp
FzeropD (double x, lisp lx)
//#line 88 "../src/num-arith.d"
{return boole (!x);}

static inline lisp
Fzeropc (const lcomplex *x, lisp lx)
//#line 89 "../src/num-arith.d"
{return boole (Fzerop (x->real) != Qnil && Fzerop (x->imag) != Qnil);}

//#line 80 "../src/num-arith.d"
lisp
Fzerop (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return Fzerops (xshort_int_value (x), x);
    case Tlong_int: return Fzerops (xlong_int_value (x), x);
    case Tbignum: return Fzeropb (xbignum_rep (x), x);
    case Tfraction: return Fzeropr ((lfraction *) (x), x);
    case Tsingle_float: return FzeropF (xsingle_float_value (x), x);
    case Tdouble_float: return FzeropD (xdouble_float_value (x), x);
    case Tcomplex: return Fzeropc ((lcomplex *) (x), x);
    }
}

static inline lisp
Fplusps (long x, lisp lx)
//#line 95 "../src/num-arith.d"
{return boole (x > 0);}

static inline lisp
Fpluspb (const bignum_rep *x, lisp lx)
//#line 97 "../src/num-arith.d"
{return boole (!x->zerop () && x->plusp ());}

static inline lisp
Fpluspr (const lfraction *x, lisp lx)
//#line 98 "../src/num-arith.d"
{return Fplusp (x->num);}

static inline lisp
FpluspF (float x, lisp lx)
//#line 99 "../src/num-arith.d"
{return boole (x > 0);}

static inline lisp
FpluspD (double x, lisp lx)
//#line 100 "../src/num-arith.d"
{return boole (x > 0);}

//#line 92 "../src/num-arith.d"
lisp
Fplusp (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return Fplusps (xshort_int_value (x), x);
    case Tlong_int: return Fplusps (xlong_int_value (x), x);
    case Tbignum: return Fpluspb (xbignum_rep (x), x);
    case Tfraction: return Fpluspr ((lfraction *) (x), x);
    case Tsingle_float: return FpluspF (xsingle_float_value (x), x);
    case Tdouble_float: return FpluspD (xdouble_float_value (x), x);
    }
}

static inline lisp
Fminusps (long x, lisp lx)
//#line 106 "../src/num-arith.d"
{return boole (x < 0);}

static inline lisp
Fminuspb (const bignum_rep *x, lisp lx)
//#line 108 "../src/num-arith.d"
{return boole (x->minusp ());}

static inline lisp
Fminuspr (const lfraction *x, lisp lx)
//#line 109 "../src/num-arith.d"
{return Fminusp (x->num);}

static inline lisp
FminuspF (float x, lisp lx)
//#line 110 "../src/num-arith.d"
{return boole (x < 0);}

static inline lisp
FminuspD (double x, lisp lx)
//#line 111 "../src/num-arith.d"
{return boole (x < 0);}

//#line 103 "../src/num-arith.d"
lisp
Fminusp (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return Fminusps (xshort_int_value (x), x);
    case Tlong_int: return Fminusps (xlong_int_value (x), x);
    case Tbignum: return Fminuspb (xbignum_rep (x), x);
    case Tfraction: return Fminuspr ((lfraction *) (x), x);
    case Tsingle_float: return FminuspF (xsingle_float_value (x), x);
    case Tdouble_float: return FminuspD (xdouble_float_value (x), x);
    }
}

static inline lisp
Foddps (long x, lisp lx)
//#line 117 "../src/num-arith.d"
{return boole (x & 1);}

static inline lisp
Foddpb (const bignum_rep *x, lisp lx)
//#line 119 "../src/num-arith.d"
{return boole (x->oddp ());}

//#line 114 "../src/num-arith.d"
lisp
Foddp (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP: return Foddps (xshort_int_value (x), x);
    case Tlong_int: return Foddps (xlong_int_value (x), x);
    case Tbignum: return Foddpb (xbignum_rep (x), x);
    }
}

static inline lisp
Fevenps (long x, lisp lx)
//#line 125 "../src/num-arith.d"
{return boole (!(x & 1));}

static inline lisp
Fevenpb (const bignum_rep *x, lisp lx)
//#line 127 "../src/num-arith.d"
{return boole (x->evenp ());}

//#line 122 "../src/num-arith.d"
lisp
Fevenp (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP: return Fevenps (xshort_int_value (x), x);
    case Tlong_int: return Fevenps (xlong_int_value (x), x);
    case Tbignum: return Fevenpb (xbignum_rep (x), x);
    }
}


//#line 130 "../src/num-arith.d"
// 12.3
static inline int
number_compareDD (double x, lisp lx, double y, lisp ly)
//#line 135 "../src/num-arith.d"
{
  if (x < y)
    return -1;
  if (x > y)
    return 1;
  return 0;
}

static inline int
number_comparess (long x, lisp lx, long y, lisp ly)
//#line 142 "../src/num-arith.d"
{
  if (x < y)
    return -1;
  if (x > y)
    return 1;
  return 0;
}

static inline int
number_comparesb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 150 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return br_compare (&xx, y);
}

static inline int
number_comparesr (long x, lisp lx, const lfraction *y, lisp ly)
//#line 154 "../src/num-arith.d"
{return number_compare (number_multiply (lx, y->den), y->num);}

static inline int
number_comparebs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 163 "../src/num-arith.d"
{return -number_comparesb (y, ly, x, lx);}

static inline int
number_comparebb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 165 "../src/num-arith.d"
{return br_compare (x, y);}

static inline int
number_comparebr (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly)
//#line 166 "../src/num-arith.d"
{return number_comparesr (0, lx, y, ly);}

static inline int
number_comparebD (const bignum_rep *x, lisp lx, double y, lisp ly)
//#line 167 "../src/num-arith.d"
{return number_compareDD (x->to_double (), lx, y, ly);}

static inline int
number_comparers (const lfraction *x, lisp lx, long y, lisp ly)
//#line 169 "../src/num-arith.d"
{return number_compare (x->num, number_multiply (x->den, ly));}

static inline int
number_comparerb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 171 "../src/num-arith.d"
{return number_comparers (x, lx, 0, ly);}

static inline int
number_comparerr (const lfraction *x, lisp lx, const lfraction *y, lisp ly)
//#line 172 "../src/num-arith.d"
{
  int xplus = Fminusp (x->num) == Qnil;
  int yplus = Fminusp (y->num) == Qnil;
  if (xplus != yplus)
    return xplus - yplus;
  return number_compare (number_multiply (x->num, y->den),
                         number_multiply (y->num, x->den));
}

static inline int
number_comparerD (const lfraction *x, lisp lx, double y, lisp ly)
//#line 180 "../src/num-arith.d"
{return number_compareDD (fract_to_double_float (x), lx, y, ly);}

static inline int
number_compareDs (double x, lisp lx, long y, lisp ly)
//#line 182 "../src/num-arith.d"
{return number_compareDD (x, lx, y, ly);}

static inline int
number_compareDb (double x, lisp lx, const bignum_rep *y, lisp ly)
//#line 184 "../src/num-arith.d"
{return number_compareDD (x, lx, y->to_double (), ly);}

static inline int
number_compareDr (double x, lisp lx, const lfraction *y, lisp ly)
//#line 185 "../src/num-arith.d"
{return number_compareDD (x, lx, fract_to_double_float (y), ly);}

static inline int
number_comparesc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 193 "../src/num-arith.d"
{return complex_compare (lx, make_fixnum (0), y->real, y->imag);}

static inline int
number_comparelc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 194 "../src/num-arith.d"
{return complex_compare (lx, make_fixnum (0), y->real, y->imag);}

static inline int
number_comparebc (const bignum_rep *x, lisp lx, const lcomplex *y, lisp ly)
//#line 195 "../src/num-arith.d"
{return complex_compare (lx, make_fixnum (0), y->real, y->imag);}

static inline int
number_comparerc (const lfraction *x, lisp lx, const lcomplex *y, lisp ly)
//#line 196 "../src/num-arith.d"
{return complex_compare (lx, make_fixnum (0), y->real, y->imag);}

static inline int
number_compareFc (float x, lisp lx, const lcomplex *y, lisp ly)
//#line 197 "../src/num-arith.d"
{return complex_compare (lx, make_fixnum (0), y->real, y->imag);}

static inline int
number_compareDc (double x, lisp lx, const lcomplex *y, lisp ly)
//#line 198 "../src/num-arith.d"
{return complex_compare (lx, make_fixnum (0), y->real, y->imag);}

static inline int
number_comparecc (const lcomplex *x, lisp lx, const lcomplex *y, lisp ly)
//#line 199 "../src/num-arith.d"
{return complex_compare (x->real, x->imag, y->real, y->imag);}

static inline int
number_comparecs (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 200 "../src/num-arith.d"
{
  return number_comparesc (y, ly, x, lx);
}

static inline int
number_comparecl (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 201 "../src/num-arith.d"
{
  return number_comparelc (y, ly, x, lx);
}

static inline int
number_comparecb (const lcomplex *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 202 "../src/num-arith.d"
{
  return number_comparebc (y, ly, x, lx);
}

static inline int
number_comparecr (const lcomplex *x, lisp lx, const lfraction *y, lisp ly)
//#line 203 "../src/num-arith.d"
{
  return number_comparerc (y, ly, x, lx);
}

static inline int
number_comparecF (const lcomplex *x, lisp lx, float y, lisp ly)
//#line 204 "../src/num-arith.d"
{
  return number_compareFc (y, ly, x, lx);
}

static inline int
number_comparecD (const lcomplex *x, lisp lx, double y, lisp ly)
//#line 205 "../src/num-arith.d"
{
  return number_compareDc (y, ly, x, lx);
}

//#line 132 "../src/num-arith.d"
int
number_compare (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_comparess (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_comparess (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_comparesb (xshort_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_comparesr (xshort_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_compareDD (xshort_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_compareDD (xshort_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_comparesc (xshort_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_comparess (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_comparess (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_comparesb (xlong_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_comparesr (xlong_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_compareDD (xlong_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_compareDD (xlong_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_comparelc (xlong_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_comparebs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_comparebs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return number_comparebb (xbignum_rep (x), x, xbignum_rep (y), y);
        case Tfraction: return number_comparebr (xbignum_rep (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_comparebD (xbignum_rep (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_comparebD (xbignum_rep (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_comparebc (xbignum_rep (x), x, (lcomplex *) (y), y);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_comparers ((lfraction *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_comparers ((lfraction *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_comparerb ((lfraction *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_comparerr ((lfraction *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_comparerD ((lfraction *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_comparerD ((lfraction *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_comparerc ((lfraction *) (x), x, (lcomplex *) (y), y);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_compareDs (xsingle_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_compareDs (xsingle_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_compareDb (xsingle_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_compareDr (xsingle_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_compareDD (xsingle_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_compareDD (xsingle_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_compareFc (xsingle_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_compareDs (xdouble_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_compareDs (xdouble_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_compareDb (xdouble_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_compareDr (xdouble_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_compareDD (xdouble_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_compareDD (xdouble_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_compareDc (xdouble_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tcomplex:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_comparecs ((lcomplex *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_comparecl ((lcomplex *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_comparecb ((lcomplex *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_comparecr ((lcomplex *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_comparecF ((lcomplex *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_comparecD ((lcomplex *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_comparecc ((lcomplex *) (x), x, (lcomplex *) (y), y);
        }
    }
}


//#line 208 "../src/num-arith.d"
// 12.4
static inline lisp
number_negates (long x, lisp lx)
//#line 213 "../src/num-arith.d"
{return make_fixnum (-x);}

static inline lisp
number_negatel (long x, lisp lx)
//#line 214 "../src/num-arith.d"
{return make_integer (-int64_t (x));}

static inline lisp
number_negateb (const bignum_rep *x, lisp lx)
//#line 215 "../src/num-arith.d"
{return make_integer (negate (0, x));}

static inline lisp
number_negater (const lfraction *x, lisp lx)
//#line 216 "../src/num-arith.d"
{return make_ratio (number_negate (x->num), x->den);}

static inline lisp
number_negateF (float x, lisp lx)
//#line 217 "../src/num-arith.d"
{return make_single_float (-x);}

static inline lisp
number_negateD (double x, lisp lx)
//#line 218 "../src/num-arith.d"
{return make_double_float (-x);}

static inline lisp
number_negatec (const lcomplex *x, lisp lx)
//#line 219 "../src/num-arith.d"
{return make_complex (number_negate (x->real), number_negate (x->imag));}

//#line 210 "../src/num-arith.d"
lisp
number_negate (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return number_negates (xshort_int_value (x), x);
    case Tlong_int: return number_negatel (xlong_int_value (x), x);
    case Tbignum: return number_negateb (xbignum_rep (x), x);
    case Tfraction: return number_negater ((lfraction *) (x), x);
    case Tsingle_float: return number_negateF (xsingle_float_value (x), x);
    case Tdouble_float: return number_negateD (xdouble_float_value (x), x);
    case Tcomplex: return number_negatec ((lcomplex *) (x), x);
    }
}

static inline lisp
number_addss (long x, lisp lx, long y, lisp ly)
//#line 225 "../src/num-arith.d"
{return make_fixnum (x + y);}

static inline lisp
number_addsl (long x, lisp lx, long y, lisp ly)
//#line 226 "../src/num-arith.d"
{return make_integer (int64_t (x) + int64_t (y));}

static inline lisp
number_addsb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 227 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return make_integer (add (0, &xx, y, 0));
}

static inline lisp
number_addsr (long x, lisp lx, const lfraction *y, lisp ly)
//#line 231 "../src/num-arith.d"
{return make_ratio (number_add (number_multiply (lx, y->den), y->num), y->den);}

static inline lisp
number_addsF (long x, lisp lx, float y, lisp ly)
//#line 232 "../src/num-arith.d"
{return make_single_float (x + y);}

static inline lisp
number_addsD (long x, lisp lx, double y, lisp ly)
//#line 233 "../src/num-arith.d"
{return make_double_float (x + y);}

static inline lisp
number_addbs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 240 "../src/num-arith.d"
{
  return number_addsb (y, ly, x, lx);
}

static inline lisp
number_addbb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 242 "../src/num-arith.d"
{return make_integer (add (0, x, y, 0));}

static inline lisp
number_addbr (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly)
//#line 243 "../src/num-arith.d"
{return number_addsr (0, lx, y, ly);}

static inline lisp
number_addbF (const bignum_rep *x, lisp lx, float y, lisp ly)
//#line 244 "../src/num-arith.d"
{return make_single_float (float (x->to_double ()) + y);}

static inline lisp
number_addbD (const bignum_rep *x, lisp lx, double y, lisp ly)
//#line 245 "../src/num-arith.d"
{return make_double_float (x->to_double () + y);}

static inline lisp
number_addrs (const lfraction *x, lisp lx, long y, lisp ly)
//#line 246 "../src/num-arith.d"
{
  return number_addsr (y, ly, x, lx);
}

static inline lisp
number_addrl (const lfraction *x, lisp lx, long y, lisp ly)
//#line 247 "../src/num-arith.d"
{
  return number_addsr (y, ly, x, lx);
}

static inline lisp
number_addrb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 248 "../src/num-arith.d"
{
  return number_addbr (y, ly, x, lx);
}

static inline lisp
number_addrr (const lfraction *x, lisp lx, const lfraction *y, lisp ly)
//#line 249 "../src/num-arith.d"
{return make_ratio (number_add (number_multiply (x->num, y->den),
                                number_multiply (y->num, x->den)),
                    number_multiply (x->den, y->den));}

static inline lisp
number_addrF (const lfraction *x, lisp lx, float y, lisp ly)
//#line 252 "../src/num-arith.d"
{return make_single_float (fract_to_single_float (x) + y);}

static inline lisp
number_addrD (const lfraction *x, lisp lx, double y, lisp ly)
//#line 253 "../src/num-arith.d"
{return make_double_float (fract_to_double_float (x) + y);}

static inline lisp
number_addFs (float x, lisp lx, long y, lisp ly)
//#line 254 "../src/num-arith.d"
{
  return number_addsF (y, ly, x, lx);
}

static inline lisp
number_addFb (float x, lisp lx, const bignum_rep *y, lisp ly)
//#line 256 "../src/num-arith.d"
{
  return number_addbF (y, ly, x, lx);
}

static inline lisp
number_addFr (float x, lisp lx, const lfraction *y, lisp ly)
//#line 257 "../src/num-arith.d"
{
  return number_addrF (y, ly, x, lx);
}

static inline lisp
number_addFF (float x, lisp lx, float y, lisp ly)
//#line 258 "../src/num-arith.d"
{return make_single_float (x + y);}

static inline lisp
number_addFD (float x, lisp lx, double y, lisp ly)
//#line 259 "../src/num-arith.d"
{return make_double_float (x + y);}

static inline lisp
number_addDs (double x, lisp lx, long y, lisp ly)
//#line 260 "../src/num-arith.d"
{
  return number_addsD (y, ly, x, lx);
}

static inline lisp
number_addDb (double x, lisp lx, const bignum_rep *y, lisp ly)
//#line 262 "../src/num-arith.d"
{
  return number_addbD (y, ly, x, lx);
}

static inline lisp
number_addDr (double x, lisp lx, const lfraction *y, lisp ly)
//#line 263 "../src/num-arith.d"
{
  return number_addrD (y, ly, x, lx);
}

static inline lisp
number_addDF (double x, lisp lx, float y, lisp ly)
//#line 264 "../src/num-arith.d"
{return make_double_float (x + y);}

static inline lisp
number_addDD (double x, lisp lx, double y, lisp ly)
//#line 265 "../src/num-arith.d"
{return make_double_float (x + y);}

static inline lisp
number_addsc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 266 "../src/num-arith.d"
{return complex_add (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_addlc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 267 "../src/num-arith.d"
{return complex_add (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_addbc (const bignum_rep *x, lisp lx, const lcomplex *y, lisp ly)
//#line 268 "../src/num-arith.d"
{return complex_add (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_addrc (const lfraction *x, lisp lx, const lcomplex *y, lisp ly)
//#line 269 "../src/num-arith.d"
{return complex_add (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_addFc (float x, lisp lx, const lcomplex *y, lisp ly)
//#line 270 "../src/num-arith.d"
{return complex_add (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_addDc (double x, lisp lx, const lcomplex *y, lisp ly)
//#line 271 "../src/num-arith.d"
{return complex_add (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_addcc (const lcomplex *x, lisp lx, const lcomplex *y, lisp ly)
//#line 272 "../src/num-arith.d"
{return complex_add (x->real, x->imag, y->real, y->imag);}

static inline lisp
number_addcs (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 273 "../src/num-arith.d"
{
  return number_addsc (y, ly, x, lx);
}

static inline lisp
number_addcl (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 274 "../src/num-arith.d"
{
  return number_addlc (y, ly, x, lx);
}

static inline lisp
number_addcb (const lcomplex *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 275 "../src/num-arith.d"
{
  return number_addbc (y, ly, x, lx);
}

static inline lisp
number_addcr (const lcomplex *x, lisp lx, const lfraction *y, lisp ly)
//#line 276 "../src/num-arith.d"
{
  return number_addrc (y, ly, x, lx);
}

static inline lisp
number_addcF (const lcomplex *x, lisp lx, float y, lisp ly)
//#line 277 "../src/num-arith.d"
{
  return number_addFc (y, ly, x, lx);
}

static inline lisp
number_addcD (const lcomplex *x, lisp lx, double y, lisp ly)
//#line 278 "../src/num-arith.d"
{
  return number_addDc (y, ly, x, lx);
}

//#line 222 "../src/num-arith.d"
lisp
number_add (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_addss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_addsl (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_addsb (xshort_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_addsr (xshort_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_addsF (xshort_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_addsD (xshort_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_addsc (xshort_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_addsl (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_addsl (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_addsb (xlong_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_addsr (xlong_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_addsF (xlong_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_addsD (xlong_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_addlc (xlong_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_addbs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_addbs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return number_addbb (xbignum_rep (x), x, xbignum_rep (y), y);
        case Tfraction: return number_addbr (xbignum_rep (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_addbF (xbignum_rep (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_addbD (xbignum_rep (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_addbc (xbignum_rep (x), x, (lcomplex *) (y), y);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_addrs ((lfraction *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_addrl ((lfraction *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_addrb ((lfraction *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_addrr ((lfraction *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_addrF ((lfraction *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_addrD ((lfraction *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_addrc ((lfraction *) (x), x, (lcomplex *) (y), y);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_addFs (xsingle_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_addFs (xsingle_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_addFb (xsingle_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_addFr (xsingle_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_addFF (xsingle_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_addFD (xsingle_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_addFc (xsingle_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_addDs (xdouble_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_addDs (xdouble_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_addDb (xdouble_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_addDr (xdouble_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_addDF (xdouble_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_addDD (xdouble_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_addDc (xdouble_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tcomplex:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_addcs ((lcomplex *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_addcl ((lcomplex *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_addcb ((lcomplex *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_addcr ((lcomplex *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_addcF ((lcomplex *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_addcD ((lcomplex *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_addcc ((lcomplex *) (x), x, (lcomplex *) (y), y);
        }
    }
}

static inline lisp
number_subtractss (long x, lisp lx, long y, lisp ly)
//#line 284 "../src/num-arith.d"
{return make_fixnum (x - y);}

static inline lisp
number_subtractsl (long x, lisp lx, long y, lisp ly)
//#line 285 "../src/num-arith.d"
{return make_integer (int64_t (x) - int64_t (y));}

static inline lisp
number_subtractsb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 286 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return make_integer (add (0, &xx, y, 1));
}

static inline lisp
number_subtractsr (long x, lisp lx, const lfraction *y, lisp ly)
//#line 290 "../src/num-arith.d"
{return make_ratio (number_subtract (number_multiply (lx, y->den), y->num), y->den);}

static inline lisp
number_subtractsF (long x, lisp lx, float y, lisp ly)
//#line 291 "../src/num-arith.d"
{return make_single_float (x - y);}

static inline lisp
number_subtractsD (long x, lisp lx, double y, lisp ly)
//#line 292 "../src/num-arith.d"
{return make_double_float (x - y);}

static inline lisp
number_subtractbs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 299 "../src/num-arith.d"
{
  bignum_rep_long yy (y);
  return make_integer (add (0, x, &yy, 1));
}

static inline lisp
number_subtractbb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 304 "../src/num-arith.d"
{return make_integer (add (0, x, y, 1));}

static inline lisp
number_subtractbr (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly)
//#line 305 "../src/num-arith.d"
{return number_subtractsr (0, lx, y, ly);}

static inline lisp
number_subtractbF (const bignum_rep *x, lisp lx, float y, lisp ly)
//#line 306 "../src/num-arith.d"
{return make_single_float (float (x->to_double ()) - y);}

static inline lisp
number_subtractbD (const bignum_rep *x, lisp lx, double y, lisp ly)
//#line 307 "../src/num-arith.d"
{return make_double_float (x->to_double () - y);}

static inline lisp
number_subtractrs (const lfraction *x, lisp lx, long y, lisp ly)
//#line 308 "../src/num-arith.d"
{return make_ratio (number_subtract (x->num, number_multiply (x->den, ly)), x->den);}

static inline lisp
number_subtractrb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 310 "../src/num-arith.d"
{return number_subtractrs (x, lx, 0, ly);}

static inline lisp
number_subtractrr (const lfraction *x, lisp lx, const lfraction *y, lisp ly)
//#line 311 "../src/num-arith.d"
{return make_ratio (number_subtract (number_multiply (x->num, y->den),
                                     number_multiply (y->num, x->den)),
                    number_multiply (x->den, y->den));}

static inline lisp
number_subtractrF (const lfraction *x, lisp lx, float y, lisp ly)
//#line 314 "../src/num-arith.d"
{return make_single_float (fract_to_single_float (x) - y);}

static inline lisp
number_subtractrD (const lfraction *x, lisp lx, double y, lisp ly)
//#line 315 "../src/num-arith.d"
{return make_double_float (fract_to_double_float (x) - y);}

static inline lisp
number_subtractFs (float x, lisp lx, long y, lisp ly)
//#line 316 "../src/num-arith.d"
{return make_single_float (x - y);}

static inline lisp
number_subtractFb (float x, lisp lx, const bignum_rep *y, lisp ly)
//#line 318 "../src/num-arith.d"
{return make_single_float (x - float (y->to_double ()));}

static inline lisp
number_subtractFr (float x, lisp lx, const lfraction *y, lisp ly)
//#line 319 "../src/num-arith.d"
{return make_single_float (x - fract_to_single_float (y));}

static inline lisp
number_subtractFF (float x, lisp lx, float y, lisp ly)
//#line 320 "../src/num-arith.d"
{return make_single_float (x - y);}

static inline lisp
number_subtractFD (float x, lisp lx, double y, lisp ly)
//#line 321 "../src/num-arith.d"
{return make_double_float (x - y);}

static inline lisp
number_subtractDs (double x, lisp lx, long y, lisp ly)
//#line 322 "../src/num-arith.d"
{return make_double_float (x - y);}

static inline lisp
number_subtractDb (double x, lisp lx, const bignum_rep *y, lisp ly)
//#line 324 "../src/num-arith.d"
{return make_double_float (x - y->to_double ());}

static inline lisp
number_subtractDr (double x, lisp lx, const lfraction *y, lisp ly)
//#line 325 "../src/num-arith.d"
{return make_double_float (x - fract_to_double_float (y));}

static inline lisp
number_subtractDF (double x, lisp lx, float y, lisp ly)
//#line 326 "../src/num-arith.d"
{return make_double_float (x - y);}

static inline lisp
number_subtractDD (double x, lisp lx, double y, lisp ly)
//#line 327 "../src/num-arith.d"
{return make_double_float (x - y);}

static inline lisp
number_subtractsc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 328 "../src/num-arith.d"
{return complex_subtract (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_subtractlc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 329 "../src/num-arith.d"
{return complex_subtract (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_subtractbc (const bignum_rep *x, lisp lx, const lcomplex *y, lisp ly)
//#line 330 "../src/num-arith.d"
{return complex_subtract (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_subtractrc (const lfraction *x, lisp lx, const lcomplex *y, lisp ly)
//#line 331 "../src/num-arith.d"
{return complex_subtract (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_subtractFc (float x, lisp lx, const lcomplex *y, lisp ly)
//#line 332 "../src/num-arith.d"
{return complex_subtract (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_subtractDc (double x, lisp lx, const lcomplex *y, lisp ly)
//#line 333 "../src/num-arith.d"
{return complex_subtract (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_subtractcc (const lcomplex *x, lisp lx, const lcomplex *y, lisp ly)
//#line 334 "../src/num-arith.d"
{return complex_subtract (x->real, x->imag, y->real, y->imag);}

static inline lisp
number_subtractcs (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 335 "../src/num-arith.d"
{return complex_subtract (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_subtractcl (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 336 "../src/num-arith.d"
{return complex_subtract (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_subtractcb (const lcomplex *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 337 "../src/num-arith.d"
{return complex_subtract (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_subtractcr (const lcomplex *x, lisp lx, const lfraction *y, lisp ly)
//#line 338 "../src/num-arith.d"
{return complex_subtract (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_subtractcF (const lcomplex *x, lisp lx, float y, lisp ly)
//#line 339 "../src/num-arith.d"
{return complex_subtract (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_subtractcD (const lcomplex *x, lisp lx, double y, lisp ly)
//#line 340 "../src/num-arith.d"
{return complex_subtract (x->real, x->imag, ly, make_fixnum (0));}

//#line 281 "../src/num-arith.d"
lisp
number_subtract (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_subtractss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_subtractsl (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_subtractsb (xshort_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_subtractsr (xshort_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_subtractsF (xshort_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_subtractsD (xshort_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_subtractsc (xshort_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_subtractsl (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_subtractsl (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_subtractsb (xlong_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_subtractsr (xlong_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_subtractsF (xlong_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_subtractsD (xlong_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_subtractlc (xlong_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_subtractbs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_subtractbs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return number_subtractbb (xbignum_rep (x), x, xbignum_rep (y), y);
        case Tfraction: return number_subtractbr (xbignum_rep (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_subtractbF (xbignum_rep (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_subtractbD (xbignum_rep (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_subtractbc (xbignum_rep (x), x, (lcomplex *) (y), y);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_subtractrs ((lfraction *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_subtractrs ((lfraction *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_subtractrb ((lfraction *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_subtractrr ((lfraction *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_subtractrF ((lfraction *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_subtractrD ((lfraction *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_subtractrc ((lfraction *) (x), x, (lcomplex *) (y), y);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_subtractFs (xsingle_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_subtractFs (xsingle_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_subtractFb (xsingle_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_subtractFr (xsingle_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_subtractFF (xsingle_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_subtractFD (xsingle_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_subtractFc (xsingle_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_subtractDs (xdouble_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_subtractDs (xdouble_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_subtractDb (xdouble_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_subtractDr (xdouble_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_subtractDF (xdouble_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_subtractDD (xdouble_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_subtractDc (xdouble_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tcomplex:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_subtractcs ((lcomplex *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_subtractcl ((lcomplex *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_subtractcb ((lcomplex *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_subtractcr ((lcomplex *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_subtractcF ((lcomplex *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_subtractcD ((lcomplex *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_subtractcc ((lcomplex *) (x), x, (lcomplex *) (y), y);
        }
    }
}

static inline lisp
number_multiplyss (long x, lisp lx, long y, lisp ly)
//#line 346 "../src/num-arith.d"
{return make_integer (int64_t (x) * int64_t (y));}

static inline lisp
number_multiplysb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 348 "../src/num-arith.d"
{
  if (x == 1)
    return ly;
  bignum_rep_long xx (x);
  return make_integer (multiply (0, &xx, y));
}

static inline lisp
number_multiplysr (long x, lisp lx, const lfraction *y, lisp ly)
//#line 354 "../src/num-arith.d"
{return x == 1 ? ly : make_ratio (number_multiply (lx, y->num), y->den);}

static inline lisp
number_multiplysF (long x, lisp lx, float y, lisp ly)
//#line 355 "../src/num-arith.d"
{return make_single_float (x * y);}

static inline lisp
number_multiplysD (long x, lisp lx, double y, lisp ly)
//#line 356 "../src/num-arith.d"
{return make_double_float (x * y);}

static inline lisp
number_multiplybs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 363 "../src/num-arith.d"
{
  return number_multiplysb (y, ly, x, lx);
}

static inline lisp
number_multiplybb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 365 "../src/num-arith.d"
{return make_integer (multiply (0, x, y));}

static inline lisp
number_multiplybr (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly)
//#line 366 "../src/num-arith.d"
{return number_multiplysr (0, lx, y, ly);}

static inline lisp
number_multiplybF (const bignum_rep *x, lisp lx, float y, lisp ly)
//#line 367 "../src/num-arith.d"
{return make_single_float (float (x->to_double ()) * y);}

static inline lisp
number_multiplybD (const bignum_rep *x, lisp lx, double y, lisp ly)
//#line 368 "../src/num-arith.d"
{return make_double_float (x->to_double () * y);}

static inline lisp
number_multiplyrs (const lfraction *x, lisp lx, long y, lisp ly)
//#line 369 "../src/num-arith.d"
{
  return number_multiplysr (y, ly, x, lx);
}

static inline lisp
number_multiplyrb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 371 "../src/num-arith.d"
{
  return number_multiplybr (y, ly, x, lx);
}

static inline lisp
number_multiplyrr (const lfraction *x, lisp lx, const lfraction *y, lisp ly)
//#line 372 "../src/num-arith.d"
{return make_ratio (number_multiply (x->num, y->num), number_multiply (x->den, y->den));}

static inline lisp
number_multiplyrF (const lfraction *x, lisp lx, float y, lisp ly)
//#line 373 "../src/num-arith.d"
{return make_single_float (fract_to_single_float (x) * y);}

static inline lisp
number_multiplyrD (const lfraction *x, lisp lx, double y, lisp ly)
//#line 374 "../src/num-arith.d"
{return make_double_float (fract_to_double_float (x) * y);}

static inline lisp
number_multiplyFs (float x, lisp lx, long y, lisp ly)
//#line 375 "../src/num-arith.d"
{
  return number_multiplysF (y, ly, x, lx);
}

static inline lisp
number_multiplyFb (float x, lisp lx, const bignum_rep *y, lisp ly)
//#line 377 "../src/num-arith.d"
{
  return number_multiplybF (y, ly, x, lx);
}

static inline lisp
number_multiplyFr (float x, lisp lx, const lfraction *y, lisp ly)
//#line 378 "../src/num-arith.d"
{
  return number_multiplyrF (y, ly, x, lx);
}

static inline lisp
number_multiplyFF (float x, lisp lx, float y, lisp ly)
//#line 379 "../src/num-arith.d"
{return make_single_float (x * y);}

static inline lisp
number_multiplyFD (float x, lisp lx, double y, lisp ly)
//#line 380 "../src/num-arith.d"
{return make_double_float (x * y);}

static inline lisp
number_multiplyDs (double x, lisp lx, long y, lisp ly)
//#line 381 "../src/num-arith.d"
{
  return number_multiplysD (y, ly, x, lx);
}

static inline lisp
number_multiplyDb (double x, lisp lx, const bignum_rep *y, lisp ly)
//#line 383 "../src/num-arith.d"
{
  return number_multiplybD (y, ly, x, lx);
}

static inline lisp
number_multiplyDr (double x, lisp lx, const lfraction *y, lisp ly)
//#line 384 "../src/num-arith.d"
{
  return number_multiplyrD (y, ly, x, lx);
}

static inline lisp
number_multiplyDF (double x, lisp lx, float y, lisp ly)
//#line 385 "../src/num-arith.d"
{
  return number_multiplyFD (y, ly, x, lx);
}

static inline lisp
number_multiplyDD (double x, lisp lx, double y, lisp ly)
//#line 386 "../src/num-arith.d"
{return make_double_float (x * y);}

static inline lisp
number_multiplysc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 387 "../src/num-arith.d"
{return complex_multiply (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_multiplylc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 388 "../src/num-arith.d"
{return complex_multiply (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_multiplybc (const bignum_rep *x, lisp lx, const lcomplex *y, lisp ly)
//#line 389 "../src/num-arith.d"
{return complex_multiply (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_multiplyrc (const lfraction *x, lisp lx, const lcomplex *y, lisp ly)
//#line 390 "../src/num-arith.d"
{return complex_multiply (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_multiplyFc (float x, lisp lx, const lcomplex *y, lisp ly)
//#line 391 "../src/num-arith.d"
{return complex_multiply (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_multiplyDc (double x, lisp lx, const lcomplex *y, lisp ly)
//#line 392 "../src/num-arith.d"
{return complex_multiply (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_multiplycc (const lcomplex *x, lisp lx, const lcomplex *y, lisp ly)
//#line 393 "../src/num-arith.d"
{return complex_multiply (x->real, x->imag, y->real, y->imag);}

static inline lisp
number_multiplycs (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 394 "../src/num-arith.d"
{
  return number_multiplysc (y, ly, x, lx);
}

static inline lisp
number_multiplycl (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 395 "../src/num-arith.d"
{
  return number_multiplylc (y, ly, x, lx);
}

static inline lisp
number_multiplycb (const lcomplex *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 396 "../src/num-arith.d"
{
  return number_multiplybc (y, ly, x, lx);
}

static inline lisp
number_multiplycr (const lcomplex *x, lisp lx, const lfraction *y, lisp ly)
//#line 397 "../src/num-arith.d"
{
  return number_multiplyrc (y, ly, x, lx);
}

static inline lisp
number_multiplycF (const lcomplex *x, lisp lx, float y, lisp ly)
//#line 398 "../src/num-arith.d"
{
  return number_multiplyFc (y, ly, x, lx);
}

static inline lisp
number_multiplycD (const lcomplex *x, lisp lx, double y, lisp ly)
//#line 399 "../src/num-arith.d"
{
  return number_multiplyDc (y, ly, x, lx);
}

//#line 343 "../src/num-arith.d"
lisp
number_multiply (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_multiplyss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_multiplyss (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_multiplysb (xshort_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_multiplysr (xshort_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_multiplysF (xshort_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_multiplysD (xshort_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_multiplysc (xshort_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_multiplyss (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_multiplyss (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_multiplysb (xlong_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_multiplysr (xlong_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_multiplysF (xlong_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_multiplysD (xlong_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_multiplylc (xlong_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_multiplybs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_multiplybs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return number_multiplybb (xbignum_rep (x), x, xbignum_rep (y), y);
        case Tfraction: return number_multiplybr (xbignum_rep (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_multiplybF (xbignum_rep (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_multiplybD (xbignum_rep (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_multiplybc (xbignum_rep (x), x, (lcomplex *) (y), y);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_multiplyrs ((lfraction *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_multiplyrs ((lfraction *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_multiplyrb ((lfraction *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_multiplyrr ((lfraction *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_multiplyrF ((lfraction *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_multiplyrD ((lfraction *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_multiplyrc ((lfraction *) (x), x, (lcomplex *) (y), y);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_multiplyFs (xsingle_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_multiplyFs (xsingle_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_multiplyFb (xsingle_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_multiplyFr (xsingle_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_multiplyFF (xsingle_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_multiplyFD (xsingle_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_multiplyFc (xsingle_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_multiplyDs (xdouble_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_multiplyDs (xdouble_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_multiplyDb (xdouble_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_multiplyDr (xdouble_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_multiplyDF (xdouble_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_multiplyDD (xdouble_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_multiplyDc (xdouble_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tcomplex:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_multiplycs ((lcomplex *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_multiplycl ((lcomplex *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_multiplycb ((lcomplex *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_multiplycr ((lcomplex *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_multiplycF ((lcomplex *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_multiplycD ((lcomplex *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_multiplycc ((lcomplex *) (x), x, (lcomplex *) (y), y);
        }
    }
}

static inline lisp
number_dividess (long x, lisp lx, long y, lisp ly)
//#line 405 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_ratio (lx, ly);
}

static inline lisp
number_dividesb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 411 "../src/num-arith.d"
{
  if (y->zerop ())
    FEdivision_by_zero ();
  return make_ratio (lx, ly);
}

static inline lisp
number_dividesr (long x, lisp lx, const lfraction *y, lisp ly)
//#line 416 "../src/num-arith.d"
{return make_ratio (number_multiply (lx, y->den), y->num);}

static inline lisp
number_dividesF (long x, lisp lx, float y, lisp ly)
//#line 417 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_single_float (x / y);
}

static inline lisp
number_dividesD (long x, lisp lx, double y, lisp ly)
//#line 422 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_double_float (x / y);
}

static inline lisp
number_dividebs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 433 "../src/num-arith.d"
{return number_dividess (0, lx, y, ly);}

static inline lisp
number_dividebb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 435 "../src/num-arith.d"
{return number_dividesb (0, lx, y, ly);}

static inline lisp
number_dividebr (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly)
//#line 436 "../src/num-arith.d"
{return number_dividesr (0, lx, y, ly);}

static inline lisp
number_dividebF (const bignum_rep *x, lisp lx, float y, lisp ly)
//#line 437 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_single_float (float (x->to_double ()) / y);
}

static inline lisp
number_dividebD (const bignum_rep *x, lisp lx, double y, lisp ly)
//#line 442 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_double_float (x->to_double () / y);
}

static inline lisp
number_dividers (const lfraction *x, lisp lx, long y, lisp ly)
//#line 447 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_ratio (x->num, number_multiply (ly, x->den));
}

static inline lisp
number_dividerb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 453 "../src/num-arith.d"
{
  if (y->zerop ())
    FEdivision_by_zero ();
  return make_ratio (x->num, number_multiply (ly, x->den));
}

static inline lisp
number_dividerr (const lfraction *x, lisp lx, const lfraction *y, lisp ly)
//#line 458 "../src/num-arith.d"
{return make_ratio (number_multiply (x->num, y->den),
                    number_multiply (x->den, y->num));}

static inline lisp
number_dividerF (const lfraction *x, lisp lx, float y, lisp ly)
//#line 460 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_single_float (fract_to_single_float (x) / y);
}

static inline lisp
number_dividerD (const lfraction *x, lisp lx, double y, lisp ly)
//#line 465 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_double_float (fract_to_double_float (x) / y);
}

static inline lisp
number_divideFs (float x, lisp lx, long y, lisp ly)
//#line 470 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_single_float (x / y);
}

static inline lisp
number_divideFb (float x, lisp lx, const bignum_rep *y, lisp ly)
//#line 476 "../src/num-arith.d"
{
  if (y->zerop ())
    FEdivision_by_zero ();
  return make_single_float (x / float (y->to_double ()));
}

static inline lisp
number_divideFr (float x, lisp lx, const lfraction *y, lisp ly)
//#line 481 "../src/num-arith.d"
{return make_single_float (x / fract_to_single_float (y));}

static inline lisp
number_divideFF (float x, lisp lx, float y, lisp ly)
//#line 482 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_single_float (x / y);
}

static inline lisp
number_divideFD (float x, lisp lx, double y, lisp ly)
//#line 487 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_double_float (x / y);
}

static inline lisp
number_divideDs (double x, lisp lx, long y, lisp ly)
//#line 492 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_double_float (x / y);
}

static inline lisp
number_divideDb (double x, lisp lx, const bignum_rep *y, lisp ly)
//#line 498 "../src/num-arith.d"
{
  if (y->zerop ())
    FEdivision_by_zero ();
  return make_double_float (x / y->to_double ());
}

static inline lisp
number_divideDr (double x, lisp lx, const lfraction *y, lisp ly)
//#line 503 "../src/num-arith.d"
{return make_double_float (x / fract_to_double_float (y));}

static inline lisp
number_divideDD (double x, lisp lx, double y, lisp ly)
//#line 504 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_double_float (x / y);
}

static inline lisp
number_dividesc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 510 "../src/num-arith.d"
{return complex_divide (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_dividelc (long x, lisp lx, const lcomplex *y, lisp ly)
//#line 511 "../src/num-arith.d"
{return complex_divide (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_dividebc (const bignum_rep *x, lisp lx, const lcomplex *y, lisp ly)
//#line 512 "../src/num-arith.d"
{return complex_divide (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_dividerc (const lfraction *x, lisp lx, const lcomplex *y, lisp ly)
//#line 513 "../src/num-arith.d"
{return complex_divide (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_divideFc (float x, lisp lx, const lcomplex *y, lisp ly)
//#line 514 "../src/num-arith.d"
{return complex_divide (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_divideDc (double x, lisp lx, const lcomplex *y, lisp ly)
//#line 515 "../src/num-arith.d"
{return complex_divide (lx, make_fixnum (0), y->real, y->imag);}

static inline lisp
number_dividecc (const lcomplex *x, lisp lx, const lcomplex *y, lisp ly)
//#line 516 "../src/num-arith.d"
{return complex_divide (x->real, x->imag, y->real, y->imag);}

static inline lisp
number_dividecs (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 517 "../src/num-arith.d"
{return complex_divide (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_dividecl (const lcomplex *x, lisp lx, long y, lisp ly)
//#line 518 "../src/num-arith.d"
{return complex_divide (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_dividecb (const lcomplex *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 519 "../src/num-arith.d"
{return complex_divide (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_dividecr (const lcomplex *x, lisp lx, const lfraction *y, lisp ly)
//#line 520 "../src/num-arith.d"
{return complex_divide (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_dividecF (const lcomplex *x, lisp lx, float y, lisp ly)
//#line 521 "../src/num-arith.d"
{return complex_divide (x->real, x->imag, ly, make_fixnum (0));}

static inline lisp
number_dividecD (const lcomplex *x, lisp lx, double y, lisp ly)
//#line 522 "../src/num-arith.d"
{return complex_divide (x->real, x->imag, ly, make_fixnum (0));}

//#line 402 "../src/num-arith.d"
lisp
number_divide (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_dividess (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_dividess (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_dividesb (xshort_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_dividesr (xshort_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_dividesF (xshort_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_dividesD (xshort_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_dividesc (xshort_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_dividess (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_dividess (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_dividesb (xlong_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_dividesr (xlong_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_dividesF (xlong_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_dividesD (xlong_int_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_dividelc (xlong_int_value (x), x, (lcomplex *) (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_dividebs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_dividebs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return number_dividebb (xbignum_rep (x), x, xbignum_rep (y), y);
        case Tfraction: return number_dividebr (xbignum_rep (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_dividebF (xbignum_rep (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_dividebD (xbignum_rep (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_dividebc (xbignum_rep (x), x, (lcomplex *) (y), y);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_dividers ((lfraction *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_dividers ((lfraction *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_dividerb ((lfraction *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_dividerr ((lfraction *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_dividerF ((lfraction *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_dividerD ((lfraction *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_dividerc ((lfraction *) (x), x, (lcomplex *) (y), y);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_divideFs (xsingle_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_divideFs (xsingle_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_divideFb (xsingle_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_divideFr (xsingle_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_divideFF (xsingle_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_divideFD (xsingle_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_divideFc (xsingle_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_divideDs (xdouble_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_divideDs (xdouble_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_divideDb (xdouble_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return number_divideDr (xdouble_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_divideDD (xdouble_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_divideDD (xdouble_float_value (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_divideDc (xdouble_float_value (x), x, (lcomplex *) (y), y);
        }
    case Tcomplex:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qnumber);
          /*NOTREACHED*/
        case Tshort_intP: return number_dividecs ((lcomplex *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_dividecl ((lcomplex *) (x), x, xlong_int_value (y), y);
        case Tbignum: return number_dividecb ((lcomplex *) (x), x, xbignum_rep (y), y);
        case Tfraction: return number_dividecr ((lcomplex *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return number_dividecF ((lcomplex *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return number_dividecD ((lcomplex *) (x), x, xdouble_float_value (y), y);
        case Tcomplex: return number_dividecc ((lcomplex *) (x), x, (lcomplex *) (y), y);
        }
    }
}

static inline lisp
integer_dividess (long x, lisp lx, long y, lisp ly)
//#line 528 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  return make_fixnum (x / y);
}

static inline lisp
integer_dividesb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 534 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return make_integer (divide (0, &xx, y));
}

static inline lisp
integer_dividels (long x, lisp lx, long y, lisp ly)
//#line 538 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  if (y == -1)
    return make_integer (-int64_t (x));
  return make_integer (int64_t (x / y));
}

static inline lisp
integer_dividebs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 547 "../src/num-arith.d"
{
  bignum_rep_long yy (y);
  return make_integer (divide (0, x, &yy));
}

static inline lisp
integer_dividebb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 552 "../src/num-arith.d"
{return make_integer (divide (0, x, y));}

//#line 525 "../src/num-arith.d"
lisp
integer_divide (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return integer_dividess (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return integer_dividess (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return integer_dividesb (xshort_int_value (x), x, xbignum_rep (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return integer_dividels (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return integer_dividess (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return integer_dividesb (xlong_int_value (x), x, xbignum_rep (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return integer_dividebs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return integer_dividebs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return integer_dividebb (xbignum_rep (x), x, xbignum_rep (y), y);
        }
    }
}

static inline lisp
Fconjugates (long x, lisp lx)
//#line 558 "../src/num-arith.d"
{return lx;}

static inline lisp
Fconjugatel (long x, lisp lx)
//#line 559 "../src/num-arith.d"
{return lx;}

static inline lisp
Fconjugateb (const bignum_rep *x, lisp lx)
//#line 560 "../src/num-arith.d"
{return lx;}

static inline lisp
Fconjugater (const lfraction *x, lisp lx)
//#line 561 "../src/num-arith.d"
{return lx;}

static inline lisp
FconjugateF (float x, lisp lx)
//#line 562 "../src/num-arith.d"
{return lx;}

static inline lisp
FconjugateD (double x, lisp lx)
//#line 563 "../src/num-arith.d"
{return lx;}

static inline lisp
Fconjugatec (const lcomplex *x, lisp lx)
//#line 564 "../src/num-arith.d"
{return make_complex (x->real, number_negate (x->imag));}

//#line 555 "../src/num-arith.d"
lisp
Fconjugate (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return Fconjugates (xshort_int_value (x), x);
    case Tlong_int: return Fconjugatel (xlong_int_value (x), x);
    case Tbignum: return Fconjugateb (xbignum_rep (x), x);
    case Tfraction: return Fconjugater ((lfraction *) (x), x);
    case Tsingle_float: return FconjugateF (xsingle_float_value (x), x);
    case Tdouble_float: return FconjugateD (xdouble_float_value (x), x);
    case Tcomplex: return Fconjugatec ((lcomplex *) (x), x);
    }
}

static inline lisp
number_lcmss (long x, lisp lx, long y, lisp ly)
//#line 570 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  bignum_rep_long yy (y);
  return make_integer (lcm (&xx, &yy));
}

static inline lisp
number_lcmsb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 576 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return make_integer (lcm (&xx, y));
}

static inline lisp
number_lcmbs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 583 "../src/num-arith.d"
{
  return number_lcmsb (y, ly, x, lx);
}

static inline lisp
number_lcmbb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 585 "../src/num-arith.d"
{return make_integer (lcm (x, y));}

//#line 567 "../src/num-arith.d"
lisp
number_lcm (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_lcmss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_lcmss (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_lcmsb (xshort_int_value (x), x, xbignum_rep (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_lcmss (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_lcmss (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_lcmsb (xlong_int_value (x), x, xbignum_rep (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_lcmbs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_lcmbs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return number_lcmbb (xbignum_rep (x), x, xbignum_rep (y), y);
        }
    }
}

static inline lisp
number_gcdss (long x, lisp lx, long y, lisp ly)
//#line 591 "../src/num-arith.d"
{
  u_long i = x >= 0 ? x : -x;
  u_long j = y >= 0 ? y : -y;
  if (i < j)
    swap (i, j);
  while (j)
    {
      u_long k = j;
      j = i % j;
      i = k;
    }
  return make_integer (int64_t (i));
}

static inline lisp
number_gcdsb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 605 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return make_integer (gcd (&xx, y));
}

static inline lisp
number_gcdbs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 612 "../src/num-arith.d"
{
  return number_gcdsb (y, ly, x, lx);
}

static inline lisp
number_gcdbb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 614 "../src/num-arith.d"
{return make_integer (gcd (x, y));}

//#line 588 "../src/num-arith.d"
lisp
number_gcd (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_gcdss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_gcdss (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_gcdsb (xshort_int_value (x), x, xbignum_rep (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_gcdss (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_gcdss (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return number_gcdsb (xlong_int_value (x), x, xbignum_rep (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_gcdbs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return number_gcdbs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return number_gcdbb (xbignum_rep (x), x, xbignum_rep (y), y);
        }
    }
}


//#line 617 "../src/num-arith.d"
//12.5

//#line 619 "../src/num-arith.d"
//12.5.1

//#line 621 "../src/num-arith.d"
static lisp number_expt_integer (lisp, lisp);
static inline lisp
integer_expt_integerss (long x, lisp lx, long y, lisp ly)
//#line 626 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return make_integer (expt (0, &xx, y));
}

static inline lisp
integer_expt_integersb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 631 "../src/num-arith.d"
{return number_expt_integer (lx, ly);}

static inline lisp
integer_expt_integerbs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 635 "../src/num-arith.d"
{return make_integer (expt (0, x, y));}

static inline lisp
integer_expt_integerbb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 637 "../src/num-arith.d"
{return number_expt_integer (lx, ly);}

//#line 623 "../src/num-arith.d"
lisp
integer_expt_integer (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return integer_expt_integerss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return integer_expt_integerss (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return integer_expt_integersb (xshort_int_value (x), x, xbignum_rep (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return integer_expt_integerss (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return integer_expt_integerss (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return integer_expt_integersb (xlong_int_value (x), x, xbignum_rep (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return integer_expt_integerbs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return integer_expt_integerbs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return integer_expt_integerbb (xbignum_rep (x), x, xbignum_rep (y), y);
        }
    }
}

static inline lisp
FsqrtD (double x, lisp lx)
//#line 643 "../src/num-arith.d"
{
  if (x < 0)
    return make_complex (make_fixnum (0),
                         make_double_float (sqrt (-x)));
  return make_double_float (sqrt (x));
}

static inline lisp
FsqrtF (float x, lisp lx)
//#line 649 "../src/num-arith.d"
{
  if (x < 0)
    return make_complex (make_fixnum (0),
                         make_single_float (float (sqrt (-x))));
  return make_single_float (float (sqrt (x)));
}

static inline lisp
Fsqrts (long x, lisp lx)
//#line 655 "../src/num-arith.d"
{return FsqrtF (float (x), lx);}

static inline lisp
Fsqrtb (const bignum_rep *x, lisp lx)
//#line 657 "../src/num-arith.d"
{return FsqrtF (float (x->to_double ()), lx);}

static inline lisp
Fsqrtr (const lfraction *x, lisp lx)
//#line 658 "../src/num-arith.d"
{return FsqrtF (fract_to_single_float (x), lx);}

static inline lisp
Fsqrtc (const lcomplex *x, lisp lx)
//#line 659 "../src/num-arith.d"
{return Fexp (number_divide (number_log (lx), make_fixnum (2)));}

//#line 640 "../src/num-arith.d"
lisp
Fsqrt (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return Fsqrts (xshort_int_value (x), x);
    case Tlong_int: return Fsqrts (xlong_int_value (x), x);
    case Tbignum: return Fsqrtb (xbignum_rep (x), x);
    case Tfraction: return Fsqrtr ((lfraction *) (x), x);
    case Tsingle_float: return FsqrtF (xsingle_float_value (x), x);
    case Tdouble_float: return FsqrtD (xdouble_float_value (x), x);
    case Tcomplex: return Fsqrtc ((lcomplex *) (x), x);
    }
}

static inline lisp
Fisqrts (long x, lisp lx)
//#line 665 "../src/num-arith.d"
{
  if (!x || x == 1)
    return make_fixnum (x);
  if (x < 0)
    FErange_error (make_fixnum (x));
  int r = x >> 1;
  while (1)
    {
      int q = x / r;
      if (q >= r)
        return make_fixnum (r);
      r = (r + q) >> 1;
    }
}

static inline lisp
Fisqrtb (const bignum_rep *x, lisp lx)
//#line 680 "../src/num-arith.d"
{
  if (x->minusp ())
    FErange_error (lx);
  return make_integer (isqrt (x));
}

static inline lisp
Fisqrtr (const lfraction *x, lisp lx)
//#line 685 "../src/num-arith.d"
{return make_ratio (Fisqrt (x->num), Fisqrt (x->den));}

//#line 662 "../src/num-arith.d"
lisp
Fisqrt (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qrational);
      /*NOTREACHED*/
    case Tshort_intP: return Fisqrts (xshort_int_value (x), x);
    case Tlong_int: return Fisqrts (xlong_int_value (x), x);
    case Tbignum: return Fisqrtb (xbignum_rep (x), x);
    case Tfraction: return Fisqrtr ((lfraction *) (x), x);
    }
}


//#line 688 "../src/num-arith.d"
//12.5.2
static inline lisp
Fsignums (long x, lisp lx)
//#line 693 "../src/num-arith.d"
{
  if (x < 0)
    return make_fixnum (-1);
  if (x > 0)
    return make_fixnum (1);
  return make_fixnum (0);
}

static inline lisp
Fsignumb (const bignum_rep *x, lisp lx)
//#line 701 "../src/num-arith.d"
{return make_fixnum (x->sign ());}

static inline lisp
Fsignumr (const lfraction *x, lisp lx)
//#line 702 "../src/num-arith.d"
{return Fsignum (x->num);}

static inline lisp
FsignumF (float x, lisp lx)
//#line 703 "../src/num-arith.d"
{
  if (x < 0)
    return make_single_float (-1.0F);
  if (x > 0)
    return make_single_float (1.0F);
  return make_single_float (0.0F);
}

static inline lisp
FsignumD (double x, lisp lx)
//#line 710 "../src/num-arith.d"
{
  if (x < 0)
    return make_double_float (-1);
  if (x > 0)
    return make_double_float (1);
  return make_double_float (0);
}

static inline lisp
Fsignumc (const lcomplex *x, lisp lx)
//#line 717 "../src/num-arith.d"
{
  if (Fzerop (lx) != Qnil)
    return lx;
  return number_divide (lx, Fabs (lx));
}

//#line 690 "../src/num-arith.d"
lisp
Fsignum (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return Fsignums (xshort_int_value (x), x);
    case Tlong_int: return Fsignums (xlong_int_value (x), x);
    case Tbignum: return Fsignumb (xbignum_rep (x), x);
    case Tfraction: return Fsignumr ((lfraction *) (x), x);
    case Tsingle_float: return FsignumF (xsingle_float_value (x), x);
    case Tdouble_float: return FsignumD (xdouble_float_value (x), x);
    case Tcomplex: return Fsignumc ((lcomplex *) (x), x);
    }
}


//#line 724 "../src/num-arith.d"
//12.6
static inline lisp
Frationals (long x, lisp lx)
//#line 729 "../src/num-arith.d"
{return lx;}

static inline lisp
Frationall (long x, lisp lx)
//#line 730 "../src/num-arith.d"
{return lx;}

static inline lisp
Frationalb (const bignum_rep *x, lisp lx)
//#line 731 "../src/num-arith.d"
{return lx;}

static inline lisp
Frationalr (const lfraction *x, lisp lx)
//#line 732 "../src/num-arith.d"
{return lx;}

static inline lisp
FrationalD (double x, lisp lx)
//#line 733 "../src/num-arith.d"
{return flonum_to_rational (x);}

//#line 726 "../src/num-arith.d"
lisp
Frational (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return Frationals (xshort_int_value (x), x);
    case Tlong_int: return Frationall (xlong_int_value (x), x);
    case Tbignum: return Frationalb (xbignum_rep (x), x);
    case Tfraction: return Frationalr ((lfraction *) (x), x);
    case Tsingle_float: return FrationalD (xsingle_float_value (x), x);
    case Tdouble_float: return FrationalD (xdouble_float_value (x), x);
    }
}

static inline lisp
Frationalizes (long x, lisp lx)
//#line 740 "../src/num-arith.d"
{return lx;}

static inline lisp
Frationalizel (long x, lisp lx)
//#line 741 "../src/num-arith.d"
{return lx;}

static inline lisp
Frationalizeb (const bignum_rep *x, lisp lx)
//#line 742 "../src/num-arith.d"
{return lx;}

static inline lisp
Frationalizer (const lfraction *x, lisp lx)
//#line 743 "../src/num-arith.d"
{return lx;}

static inline lisp
FrationalizeD (double x, lisp lx)
//#line 744 "../src/num-arith.d"
{return flonum_rationalize (x, DBL_DIG);}

static inline lisp
FrationalizeF (float x, lisp lx)
//#line 745 "../src/num-arith.d"
{return flonum_rationalize (x, FLT_DIG);}

//#line 737 "../src/num-arith.d"
lisp
Frationalize (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP: return Frationalizes (xshort_int_value (x), x);
    case Tlong_int: return Frationalizel (xlong_int_value (x), x);
    case Tbignum: return Frationalizeb (xbignum_rep (x), x);
    case Tfraction: return Frationalizer ((lfraction *) (x), x);
    case Tsingle_float: return FrationalizeF (xsingle_float_value (x), x);
    case Tdouble_float: return FrationalizeD (xdouble_float_value (x), x);
    }
}

static inline lisp
Fnumerators (long x, lisp lx)
//#line 751 "../src/num-arith.d"
{return lx;}

static inline lisp
Fnumeratorl (long x, lisp lx)
//#line 752 "../src/num-arith.d"
{return lx;}

static inline lisp
Fnumeratorb (const bignum_rep *x, lisp lx)
//#line 753 "../src/num-arith.d"
{return lx;}

static inline lisp
Fnumeratorr (const lfraction *x, lisp lx)
//#line 754 "../src/num-arith.d"
{return x->num;}

//#line 748 "../src/num-arith.d"
lisp
Fnumerator (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qrational);
      /*NOTREACHED*/
    case Tshort_intP: return Fnumerators (xshort_int_value (x), x);
    case Tlong_int: return Fnumeratorl (xlong_int_value (x), x);
    case Tbignum: return Fnumeratorb (xbignum_rep (x), x);
    case Tfraction: return Fnumeratorr ((lfraction *) (x), x);
    }
}

static inline lisp
Fdenominators (long x, lisp lx)
//#line 760 "../src/num-arith.d"
{return make_fixnum (1);}

static inline lisp
Fdenominatorl (long x, lisp lx)
//#line 761 "../src/num-arith.d"
{return make_fixnum (1);}

static inline lisp
Fdenominatorb (const bignum_rep *x, lisp lx)
//#line 762 "../src/num-arith.d"
{return make_fixnum (1);}

static inline lisp
Fdenominatorr (const lfraction *x, lisp lx)
//#line 763 "../src/num-arith.d"
{return x->den;}

//#line 757 "../src/num-arith.d"
lisp
Fdenominator (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qrational);
      /*NOTREACHED*/
    case Tshort_intP: return Fdenominators (xshort_int_value (x), x);
    case Tlong_int: return Fdenominatorl (xlong_int_value (x), x);
    case Tbignum: return Fdenominatorb (xbignum_rep (x), x);
    case Tfraction: return Fdenominatorr ((lfraction *) (x), x);
    }
}

static inline lisp
fix_number_2ss (long x, lisp lx, long y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 769 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  long q, r;
  q = (*ffn)(&r, x, y);
  multiple_value::value (1) = make_fixnum (r);
  return make_fixnum (q);
}

static inline lisp
fix_number_2bb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 777 "../src/num-arith.d"
{
  if (y->zerop ())
    FEdivision_by_zero ();
  bignum_rep *q, *r;
  (*bfn)(q, r, x, y);
  safe_bignum_rep qq (q);
  multiple_value::value (1) = make_integer (r);
  qq.discard ();
  return make_integer (q);
}

static inline lisp
fix_number_2DD (double x, lisp lx, double y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 787 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  double q = (*dfn)(x / y);
  multiple_value::value (1) = make_double_float (x - y * q);
  return flonum_to_integer (q);
}

static inline lisp
fix_number_2FF (float x, lisp lx, float y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 794 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  double q = (*dfn)(x / y);
  multiple_value::value (1) = make_single_float (float (x - y * q));
  return flonum_to_integer (q);
}

static inline lisp
fix_number_2sb (long x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 802 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return fix_number_2bb (&xx, lx, y, ly, dfn, bfn, ffn);
}

static inline lisp
fix_number_2sr (long x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 806 "../src/num-arith.d"
{
  lisp q = fix_number_2 (number_multiply (lx, y->den), y->num, dfn, bfn, ffn);
  multiple_value::value (1) = make_ratio (multiple_value::value (1), y->den);
  return q;
}

static inline lisp
fix_number_2sF (long x, lisp lx, float y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 811 "../src/num-arith.d"
{return fix_number_2FF (float (x), lx, y, ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2ls (long x, lisp lx, long y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 813 "../src/num-arith.d"
{
  if (y == -1)
    {
      multiple_value::value (1) = make_fixnum (0);
      return make_integer (-int64_t (x));
    }
  return fix_number_2ss (x, lx, y, ly, dfn, bfn, ffn);
}

static inline lisp
fix_number_2bs (const bignum_rep *x, lisp lx, long y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 826 "../src/num-arith.d"
{
  bignum_rep_long yy (y);
  return fix_number_2bb (x, lx, &yy, ly, dfn, bfn, ffn);
}

static inline lisp
fix_number_2br (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 831 "../src/num-arith.d"
{return fix_number_2sr (0, lx, y, ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2bF (const bignum_rep *x, lisp lx, float y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 832 "../src/num-arith.d"
{return fix_number_2FF (float (x->to_double ()), lx, y, ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2bD (const bignum_rep *x, lisp lx, double y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 833 "../src/num-arith.d"
{return fix_number_2DD (x->to_double (), lx, y, ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2rs (const lfraction *x, lisp lx, long y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 834 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  lisp den = number_multiply (x->den, ly);
  lisp q = fix_number_2 (x->num, den, dfn, bfn, ffn);
  multiple_value::value (1) = make_ratio (multiple_value::value (1), x->den);
  return q;
}

static inline lisp
fix_number_2rb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 843 "../src/num-arith.d"
{
  if (y->zerop ())
    FEdivision_by_zero ();
  lisp den = number_multiply (x->den, ly);
  lisp q = fix_number_2 (x->num, den, dfn, bfn, ffn);
  multiple_value::value (1) = make_ratio (multiple_value::value (1), x->den);
  return q;
}

static inline lisp
fix_number_2rr (const lfraction *x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 851 "../src/num-arith.d"
{
  lisp q = fix_number_2 (number_multiply (x->num, y->den),
                         number_multiply (x->den, y->num),
                         dfn, bfn, ffn);
  multiple_value::value (1) = make_ratio (multiple_value::value (1),
                                          number_multiply (x->den, y->den));
  return q;
}

static inline lisp
fix_number_2rF (const lfraction *x, lisp lx, float y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 859 "../src/num-arith.d"
{return fix_number_2FF (fract_to_single_float (x), lx, y, ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2rD (const lfraction *x, lisp lx, double y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 860 "../src/num-arith.d"
{return fix_number_2DD (fract_to_double_float (x), lx, y, ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2Fs (float x, lisp lx, long y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 861 "../src/num-arith.d"
{return fix_number_2FF (x, lx, float (y), ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2Fb (float x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 863 "../src/num-arith.d"
{return fix_number_2FF (x, lx, float (y->to_double ()), ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2Fr (float x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 864 "../src/num-arith.d"
{return fix_number_2FF (x, lx, fract_to_single_float (y), ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2Db (double x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 868 "../src/num-arith.d"
{return fix_number_2DD (x, lx, y->to_double (), ly, dfn, bfn, ffn);}

static inline lisp
fix_number_2Dr (double x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
//#line 869 "../src/num-arith.d"
{return fix_number_2DD (x, lx, fract_to_double_float (y), ly, dfn, bfn, ffn);}

//#line 766 "../src/num-arith.d"
lisp
fix_number_2 (lisp x, lisp y,FIX_FLONUM dfn, FIX_BIGNUM bfn, FIX_FIXNUM ffn)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_number_2ss (xshort_int_value (x), x, xshort_int_value (y), y,dfn,bfn,ffn);
        case Tlong_int: return fix_number_2ss (xshort_int_value (x), x, xlong_int_value (y), y,dfn,bfn,ffn);
        case Tbignum: return fix_number_2sb (xshort_int_value (x), x, xbignum_rep (y), y,dfn,bfn,ffn);
        case Tfraction: return fix_number_2sr (xshort_int_value (x), x, (lfraction *) (y), y,dfn,bfn,ffn);
        case Tsingle_float: return fix_number_2sF (xshort_int_value (x), x, xsingle_float_value (y), y,dfn,bfn,ffn);
        case Tdouble_float: return fix_number_2DD (xshort_int_value (x), x, xdouble_float_value (y), y,dfn,bfn,ffn);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_number_2ls (xlong_int_value (x), x, xshort_int_value (y), y,dfn,bfn,ffn);
        case Tlong_int: return fix_number_2ss (xlong_int_value (x), x, xlong_int_value (y), y,dfn,bfn,ffn);
        case Tbignum: return fix_number_2sb (xlong_int_value (x), x, xbignum_rep (y), y,dfn,bfn,ffn);
        case Tfraction: return fix_number_2sr (xlong_int_value (x), x, (lfraction *) (y), y,dfn,bfn,ffn);
        case Tsingle_float: return fix_number_2sF (xlong_int_value (x), x, xsingle_float_value (y), y,dfn,bfn,ffn);
        case Tdouble_float: return fix_number_2DD (xlong_int_value (x), x, xdouble_float_value (y), y,dfn,bfn,ffn);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_number_2bs (xbignum_rep (x), x, xshort_int_value (y), y,dfn,bfn,ffn);
        case Tlong_int: return fix_number_2bs (xbignum_rep (x), x, xlong_int_value (y), y,dfn,bfn,ffn);
        case Tbignum: return fix_number_2bb (xbignum_rep (x), x, xbignum_rep (y), y,dfn,bfn,ffn);
        case Tfraction: return fix_number_2br (xbignum_rep (x), x, (lfraction *) (y), y,dfn,bfn,ffn);
        case Tsingle_float: return fix_number_2bF (xbignum_rep (x), x, xsingle_float_value (y), y,dfn,bfn,ffn);
        case Tdouble_float: return fix_number_2bD (xbignum_rep (x), x, xdouble_float_value (y), y,dfn,bfn,ffn);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_number_2rs ((lfraction *) (x), x, xshort_int_value (y), y,dfn,bfn,ffn);
        case Tlong_int: return fix_number_2rs ((lfraction *) (x), x, xlong_int_value (y), y,dfn,bfn,ffn);
        case Tbignum: return fix_number_2rb ((lfraction *) (x), x, xbignum_rep (y), y,dfn,bfn,ffn);
        case Tfraction: return fix_number_2rr ((lfraction *) (x), x, (lfraction *) (y), y,dfn,bfn,ffn);
        case Tsingle_float: return fix_number_2rF ((lfraction *) (x), x, xsingle_float_value (y), y,dfn,bfn,ffn);
        case Tdouble_float: return fix_number_2rD ((lfraction *) (x), x, xdouble_float_value (y), y,dfn,bfn,ffn);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_number_2Fs (xsingle_float_value (x), x, xshort_int_value (y), y,dfn,bfn,ffn);
        case Tlong_int: return fix_number_2Fs (xsingle_float_value (x), x, xlong_int_value (y), y,dfn,bfn,ffn);
        case Tbignum: return fix_number_2Fb (xsingle_float_value (x), x, xbignum_rep (y), y,dfn,bfn,ffn);
        case Tfraction: return fix_number_2Fr (xsingle_float_value (x), x, (lfraction *) (y), y,dfn,bfn,ffn);
        case Tsingle_float: return fix_number_2FF (xsingle_float_value (x), x, xsingle_float_value (y), y,dfn,bfn,ffn);
        case Tdouble_float: return fix_number_2DD (xsingle_float_value (x), x, xdouble_float_value (y), y,dfn,bfn,ffn);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_number_2DD (xdouble_float_value (x), x, xshort_int_value (y), y,dfn,bfn,ffn);
        case Tlong_int: return fix_number_2DD (xdouble_float_value (x), x, xlong_int_value (y), y,dfn,bfn,ffn);
        case Tbignum: return fix_number_2Db (xdouble_float_value (x), x, xbignum_rep (y), y,dfn,bfn,ffn);
        case Tfraction: return fix_number_2Dr (xdouble_float_value (x), x, (lfraction *) (y), y,dfn,bfn,ffn);
        case Tsingle_float: return fix_number_2DD (xdouble_float_value (x), x, xsingle_float_value (y), y,dfn,bfn,ffn);
        case Tdouble_float: return fix_number_2DD (xdouble_float_value (x), x, xdouble_float_value (y), y,dfn,bfn,ffn);
        }
    }
}

static inline lisp
fix_flonum_2DD (double x, lisp lx, double y, lisp ly,FIX_FLONUM dfn)
//#line 876 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  double q = (*dfn)(x / y);
  multiple_value::value (1) = make_double_float (x - y * q);
  return make_double_float (q);
}

static inline lisp
fix_flonum_2FF (float x, lisp lx, float y, lisp ly,FIX_FLONUM dfn)
//#line 883 "../src/num-arith.d"
{
  if (!y)
    FEdivision_by_zero ();
  double q = (*dfn)(x / y);
  multiple_value::value (1) = make_single_float (float (x - y * q));
  return make_single_float (float (q));
}

static inline lisp
fix_flonum_2ss (long x, lisp lx, long y, lisp ly,FIX_FLONUM dfn)
//#line 890 "../src/num-arith.d"
{return fix_flonum_2FF (float (x), lx, float (y), ly, dfn);}

static inline lisp
fix_flonum_2sb (long x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn)
//#line 892 "../src/num-arith.d"
{return fix_flonum_2FF (float (x), lx, float (y->to_double ()), ly, dfn);}

static inline lisp
fix_flonum_2sr (long x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn)
//#line 893 "../src/num-arith.d"
{return fix_flonum_2FF (float (x), lx, fract_to_single_float (y), ly, dfn);}

static inline lisp
fix_flonum_2sF (long x, lisp lx, float y, lisp ly,FIX_FLONUM dfn)
//#line 894 "../src/num-arith.d"
{return fix_flonum_2FF (float (x), lx, y, ly, dfn);}

static inline lisp
fix_flonum_2bs (const bignum_rep *x, lisp lx, long y, lisp ly,FIX_FLONUM dfn)
//#line 902 "../src/num-arith.d"
{return fix_flonum_2FF (float (x->to_double ()), lx, float (y), ly, dfn);}

static inline lisp
fix_flonum_2bb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn)
//#line 904 "../src/num-arith.d"
{return fix_flonum_2FF (float (x->to_double ()), lx,
                        float (y->to_double ()), ly, dfn);}

static inline lisp
fix_flonum_2br (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn)
//#line 906 "../src/num-arith.d"
{return fix_flonum_2FF (float (x->to_double ()), lx,
                        fract_to_single_float (y), ly, dfn);}

static inline lisp
fix_flonum_2bF (const bignum_rep *x, lisp lx, float y, lisp ly,FIX_FLONUM dfn)
//#line 908 "../src/num-arith.d"
{return fix_flonum_2FF (float (x->to_double ()), lx, y, ly, dfn);}

static inline lisp
fix_flonum_2bD (const bignum_rep *x, lisp lx, double y, lisp ly,FIX_FLONUM dfn)
//#line 909 "../src/num-arith.d"
{return fix_flonum_2DD (x->to_double (), lx, y, ly, dfn);}

static inline lisp
fix_flonum_2rs (const lfraction *x, lisp lx, long y, lisp ly,FIX_FLONUM dfn)
//#line 910 "../src/num-arith.d"
{return fix_flonum_2FF (fract_to_single_float (x), lx, float (y), ly, dfn);}

static inline lisp
fix_flonum_2rb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn)
//#line 912 "../src/num-arith.d"
{return fix_flonum_2FF (fract_to_single_float (x), lx,
                        float (y->to_double ()), ly, dfn);}

static inline lisp
fix_flonum_2rr (const lfraction *x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn)
//#line 914 "../src/num-arith.d"
{return fix_flonum_2FF (fract_to_single_float (x), lx,
                        fract_to_single_float (y), ly, dfn);}

static inline lisp
fix_flonum_2rF (const lfraction *x, lisp lx, float y, lisp ly,FIX_FLONUM dfn)
//#line 916 "../src/num-arith.d"
{return fix_flonum_2FF (fract_to_single_float (x), lx, y, ly, dfn);}

static inline lisp
fix_flonum_2rD (const lfraction *x, lisp lx, double y, lisp ly,FIX_FLONUM dfn)
//#line 917 "../src/num-arith.d"
{return fix_flonum_2DD (fract_to_double_float (x), lx, y, ly, dfn);}

static inline lisp
fix_flonum_2Fs (float x, lisp lx, long y, lisp ly,FIX_FLONUM dfn)
//#line 918 "../src/num-arith.d"
{return fix_flonum_2FF (x, lx, float (y), ly, dfn);}

static inline lisp
fix_flonum_2Fb (float x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn)
//#line 920 "../src/num-arith.d"
{return fix_flonum_2FF (x, lx, float (y->to_double ()), ly, dfn);}

static inline lisp
fix_flonum_2Fr (float x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn)
//#line 921 "../src/num-arith.d"
{return fix_flonum_2FF (x, lx, fract_to_single_float (y), ly, dfn);}

static inline lisp
fix_flonum_2Db (double x, lisp lx, const bignum_rep *y, lisp ly,FIX_FLONUM dfn)
//#line 925 "../src/num-arith.d"
{return fix_flonum_2DD (x, lx, y->to_double (), ly, dfn);}

static inline lisp
fix_flonum_2Dr (double x, lisp lx, const lfraction *y, lisp ly,FIX_FLONUM dfn)
//#line 926 "../src/num-arith.d"
{return fix_flonum_2DD (x, lx, fract_to_double_float (y), ly, dfn);}

//#line 873 "../src/num-arith.d"
lisp
fix_flonum_2 (lisp x, lisp y,FIX_FLONUM dfn)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_flonum_2ss (xshort_int_value (x), x, xshort_int_value (y), y,dfn);
        case Tlong_int: return fix_flonum_2ss (xshort_int_value (x), x, xlong_int_value (y), y,dfn);
        case Tbignum: return fix_flonum_2sb (xshort_int_value (x), x, xbignum_rep (y), y,dfn);
        case Tfraction: return fix_flonum_2sr (xshort_int_value (x), x, (lfraction *) (y), y,dfn);
        case Tsingle_float: return fix_flonum_2sF (xshort_int_value (x), x, xsingle_float_value (y), y,dfn);
        case Tdouble_float: return fix_flonum_2DD (xshort_int_value (x), x, xdouble_float_value (y), y,dfn);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_flonum_2ss (xlong_int_value (x), x, xshort_int_value (y), y,dfn);
        case Tlong_int: return fix_flonum_2ss (xlong_int_value (x), x, xlong_int_value (y), y,dfn);
        case Tbignum: return fix_flonum_2sb (xlong_int_value (x), x, xbignum_rep (y), y,dfn);
        case Tfraction: return fix_flonum_2sr (xlong_int_value (x), x, (lfraction *) (y), y,dfn);
        case Tsingle_float: return fix_flonum_2sF (xlong_int_value (x), x, xsingle_float_value (y), y,dfn);
        case Tdouble_float: return fix_flonum_2DD (xlong_int_value (x), x, xdouble_float_value (y), y,dfn);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_flonum_2bs (xbignum_rep (x), x, xshort_int_value (y), y,dfn);
        case Tlong_int: return fix_flonum_2bs (xbignum_rep (x), x, xlong_int_value (y), y,dfn);
        case Tbignum: return fix_flonum_2bb (xbignum_rep (x), x, xbignum_rep (y), y,dfn);
        case Tfraction: return fix_flonum_2br (xbignum_rep (x), x, (lfraction *) (y), y,dfn);
        case Tsingle_float: return fix_flonum_2bF (xbignum_rep (x), x, xsingle_float_value (y), y,dfn);
        case Tdouble_float: return fix_flonum_2bD (xbignum_rep (x), x, xdouble_float_value (y), y,dfn);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_flonum_2rs ((lfraction *) (x), x, xshort_int_value (y), y,dfn);
        case Tlong_int: return fix_flonum_2rs ((lfraction *) (x), x, xlong_int_value (y), y,dfn);
        case Tbignum: return fix_flonum_2rb ((lfraction *) (x), x, xbignum_rep (y), y,dfn);
        case Tfraction: return fix_flonum_2rr ((lfraction *) (x), x, (lfraction *) (y), y,dfn);
        case Tsingle_float: return fix_flonum_2rF ((lfraction *) (x), x, xsingle_float_value (y), y,dfn);
        case Tdouble_float: return fix_flonum_2rD ((lfraction *) (x), x, xdouble_float_value (y), y,dfn);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_flonum_2Fs (xsingle_float_value (x), x, xshort_int_value (y), y,dfn);
        case Tlong_int: return fix_flonum_2Fs (xsingle_float_value (x), x, xlong_int_value (y), y,dfn);
        case Tbignum: return fix_flonum_2Fb (xsingle_float_value (x), x, xbignum_rep (y), y,dfn);
        case Tfraction: return fix_flonum_2Fr (xsingle_float_value (x), x, (lfraction *) (y), y,dfn);
        case Tsingle_float: return fix_flonum_2FF (xsingle_float_value (x), x, xsingle_float_value (y), y,dfn);
        case Tdouble_float: return fix_flonum_2DD (xsingle_float_value (x), x, xdouble_float_value (y), y,dfn);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return fix_flonum_2DD (xdouble_float_value (x), x, xshort_int_value (y), y,dfn);
        case Tlong_int: return fix_flonum_2DD (xdouble_float_value (x), x, xlong_int_value (y), y,dfn);
        case Tbignum: return fix_flonum_2Db (xdouble_float_value (x), x, xbignum_rep (y), y,dfn);
        case Tfraction: return fix_flonum_2Dr (xdouble_float_value (x), x, (lfraction *) (y), y,dfn);
        case Tsingle_float: return fix_flonum_2DD (xdouble_float_value (x), x, xsingle_float_value (y), y,dfn);
        case Tdouble_float: return fix_flonum_2DD (xdouble_float_value (x), x, xdouble_float_value (y), y,dfn);
        }
    }
}

static inline lisp
Frealparts (long x, lisp lx)
//#line 933 "../src/num-arith.d"
{return lx;}

static inline lisp
Frealpartl (long x, lisp lx)
//#line 934 "../src/num-arith.d"
{return lx;}

static inline lisp
Frealpartb (const bignum_rep *x, lisp lx)
//#line 935 "../src/num-arith.d"
{return lx;}

static inline lisp
Frealpartr (const lfraction *x, lisp lx)
//#line 936 "../src/num-arith.d"
{return lx;}

static inline lisp
FrealpartF (float x, lisp lx)
//#line 937 "../src/num-arith.d"
{return lx;}

static inline lisp
FrealpartD (double x, lisp lx)
//#line 938 "../src/num-arith.d"
{return lx;}

static inline lisp
Frealpartc (const lcomplex *x, lisp lx)
//#line 939 "../src/num-arith.d"
{return x->real;}

//#line 930 "../src/num-arith.d"
lisp
Frealpart (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return Frealparts (xshort_int_value (x), x);
    case Tlong_int: return Frealpartl (xlong_int_value (x), x);
    case Tbignum: return Frealpartb (xbignum_rep (x), x);
    case Tfraction: return Frealpartr ((lfraction *) (x), x);
    case Tsingle_float: return FrealpartF (xsingle_float_value (x), x);
    case Tdouble_float: return FrealpartD (xdouble_float_value (x), x);
    case Tcomplex: return Frealpartc ((lcomplex *) (x), x);
    }
}

static inline lisp
Fimagparts (long x, lisp lx)
//#line 945 "../src/num-arith.d"
{return make_fixnum (0);}

static inline lisp
Fimagpartl (long x, lisp lx)
//#line 946 "../src/num-arith.d"
{return make_fixnum (0);}

static inline lisp
Fimagpartb (const bignum_rep *x, lisp lx)
//#line 947 "../src/num-arith.d"
{return make_fixnum (0);}

static inline lisp
Fimagpartr (const lfraction *x, lisp lx)
//#line 948 "../src/num-arith.d"
{return make_fixnum (0);}

static inline lisp
FimagpartF (float x, lisp lx)
//#line 949 "../src/num-arith.d"
{return make_single_float (0.0F);}

static inline lisp
FimagpartD (double x, lisp lx)
//#line 950 "../src/num-arith.d"
{return make_double_float (0.0);}

static inline lisp
Fimagpartc (const lcomplex *x, lisp lx)
//#line 951 "../src/num-arith.d"
{return x->imag;}

//#line 942 "../src/num-arith.d"
lisp
Fimagpart (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qnumber);
      /*NOTREACHED*/
    case Tshort_intP: return Fimagparts (xshort_int_value (x), x);
    case Tlong_int: return Fimagpartl (xlong_int_value (x), x);
    case Tbignum: return Fimagpartb (xbignum_rep (x), x);
    case Tfraction: return Fimagpartr ((lfraction *) (x), x);
    case Tsingle_float: return FimagpartF (xsingle_float_value (x), x);
    case Tdouble_float: return FimagpartD (xdouble_float_value (x), x);
    case Tcomplex: return Fimagpartc ((lcomplex *) (x), x);
    }
}

static inline lisp
make_complexss (long x, lisp lx, long y, lisp ly)
//#line 957 "../src/num-arith.d"
{
  if (!y)
    return lx;
  return make_complex_1 (lx, ly);
}

static inline lisp
make_complexsb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 963 "../src/num-arith.d"
{
  if (y->zerop ())
    return lx;
  return make_complex_1 (lx, ly);
}

static inline lisp
make_complexsr (long x, lisp lx, const lfraction *y, lisp ly)
//#line 968 "../src/num-arith.d"
{return make_complex_1 (lx, ly);}

static inline lisp
make_complexsF (long x, lisp lx, float y, lisp ly)
//#line 969 "../src/num-arith.d"
{return make_complex_1 (make_single_float (float (x)), ly);}

static inline lisp
make_complexsD (long x, lisp lx, double y, lisp ly)
//#line 970 "../src/num-arith.d"
{return make_complex_1 (make_double_float (x), ly);}

static inline lisp
make_complexbs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 977 "../src/num-arith.d"
{return make_complexss (0, lx, y, ly);}

static inline lisp
make_complexbb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 979 "../src/num-arith.d"
{return make_complexsb (0, lx, y, ly);}

static inline lisp
make_complexbr (const bignum_rep *x, lisp lx, const lfraction *y, lisp ly)
//#line 980 "../src/num-arith.d"
{return make_complex_1 (lx, ly);}

static inline lisp
make_complexbF (const bignum_rep *x, lisp lx, float y, lisp ly)
//#line 981 "../src/num-arith.d"
{return make_complex_1 (make_single_float (float (x->to_double ())), ly);}

static inline lisp
make_complexbD (const bignum_rep *x, lisp lx, double y, lisp ly)
//#line 982 "../src/num-arith.d"
{return make_complex_1 (make_double_float (x->to_double ()), ly);}

static inline lisp
make_complexrs (const lfraction *x, lisp lx, long y, lisp ly)
//#line 983 "../src/num-arith.d"
{return make_complexss (0, lx, y, ly);}

static inline lisp
make_complexrb (const lfraction *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 985 "../src/num-arith.d"
{return make_complexsb (0, lx, y, ly);}

static inline lisp
make_complexrr (const lfraction *x, lisp lx, const lfraction *y, lisp ly)
//#line 986 "../src/num-arith.d"
{return make_complex_1 (lx, ly);}

static inline lisp
make_complexrF (const lfraction *x, lisp lx, float y, lisp ly)
//#line 987 "../src/num-arith.d"
{return make_complex_1 (make_single_float (fract_to_single_float (x)), ly);}

static inline lisp
make_complexrD (const lfraction *x, lisp lx, double y, lisp ly)
//#line 988 "../src/num-arith.d"
{return make_complex_1 (make_double_float (fract_to_double_float (x)), ly);}

static inline lisp
make_complexFs (float x, lisp lx, long y, lisp ly)
//#line 989 "../src/num-arith.d"
{return make_complex_1 (lx, make_single_float (float (y)));}

static inline lisp
make_complexFb (float x, lisp lx, const bignum_rep *y, lisp ly)
//#line 991 "../src/num-arith.d"
{return make_complex_1 (lx, make_single_float (float (y->to_double ())));}

static inline lisp
make_complexFr (float x, lisp lx, const lfraction *y, lisp ly)
//#line 992 "../src/num-arith.d"
{return make_complex_1 (lx, make_single_float (fract_to_single_float (y)));}

static inline lisp
make_complexFF (float x, lisp lx, float y, lisp ly)
//#line 993 "../src/num-arith.d"
{return make_complex_1 (lx, ly);}

static inline lisp
make_complexFD (float x, lisp lx, double y, lisp ly)
//#line 994 "../src/num-arith.d"
{return make_complex_1 (make_double_float (x), ly);}

static inline lisp
make_complexDs (double x, lisp lx, long y, lisp ly)
//#line 995 "../src/num-arith.d"
{return make_complex_1 (lx, make_double_float (y));}

static inline lisp
make_complexDb (double x, lisp lx, const bignum_rep *y, lisp ly)
//#line 997 "../src/num-arith.d"
{return make_complex_1 (lx, make_double_float (y->to_double ()));}

static inline lisp
make_complexDr (double x, lisp lx, const lfraction *y, lisp ly)
//#line 998 "../src/num-arith.d"
{return make_complex_1 (lx, make_double_float (fract_to_double_float (y)));}

static inline lisp
make_complexDF (double x, lisp lx, float y, lisp ly)
//#line 999 "../src/num-arith.d"
{return make_complex_1 (lx, make_double_float (y));}

static inline lisp
make_complexDD (double x, lisp lx, double y, lisp ly)
//#line 1000 "../src/num-arith.d"
{return make_complex_1 (lx, ly);}

//#line 954 "../src/num-arith.d"
lisp
make_complex (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qreal);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return make_complexss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return make_complexss (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return make_complexsb (xshort_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return make_complexsr (xshort_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return make_complexsF (xshort_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return make_complexsD (xshort_int_value (x), x, xdouble_float_value (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return make_complexss (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return make_complexss (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return make_complexsb (xlong_int_value (x), x, xbignum_rep (y), y);
        case Tfraction: return make_complexsr (xlong_int_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return make_complexsF (xlong_int_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return make_complexsD (xlong_int_value (x), x, xdouble_float_value (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return make_complexbs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return make_complexbs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return make_complexbb (xbignum_rep (x), x, xbignum_rep (y), y);
        case Tfraction: return make_complexbr (xbignum_rep (x), x, (lfraction *) (y), y);
        case Tsingle_float: return make_complexbF (xbignum_rep (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return make_complexbD (xbignum_rep (x), x, xdouble_float_value (y), y);
        }
    case Tfraction:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return make_complexrs ((lfraction *) (x), x, xshort_int_value (y), y);
        case Tlong_int: return make_complexrs ((lfraction *) (x), x, xlong_int_value (y), y);
        case Tbignum: return make_complexrb ((lfraction *) (x), x, xbignum_rep (y), y);
        case Tfraction: return make_complexrr ((lfraction *) (x), x, (lfraction *) (y), y);
        case Tsingle_float: return make_complexrF ((lfraction *) (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return make_complexrD ((lfraction *) (x), x, xdouble_float_value (y), y);
        }
    case Tsingle_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return make_complexFs (xsingle_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return make_complexFs (xsingle_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return make_complexFb (xsingle_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return make_complexFr (xsingle_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return make_complexFF (xsingle_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return make_complexFD (xsingle_float_value (x), x, xdouble_float_value (y), y);
        }
    case Tdouble_float:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qreal);
          /*NOTREACHED*/
        case Tshort_intP: return make_complexDs (xdouble_float_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return make_complexDs (xdouble_float_value (x), x, xlong_int_value (y), y);
        case Tbignum: return make_complexDb (xdouble_float_value (x), x, xbignum_rep (y), y);
        case Tfraction: return make_complexDr (xdouble_float_value (x), x, (lfraction *) (y), y);
        case Tsingle_float: return make_complexDF (xdouble_float_value (x), x, xsingle_float_value (y), y);
        case Tdouble_float: return make_complexDD (xdouble_float_value (x), x, xdouble_float_value (y), y);
        }
    }
}


//#line 1003 "../src/num-arith.d"
//12.7

//#line 1005 "../src/num-arith.d"
static lisp fixnum_logope (long x, long y, logope_code ope);
static inline lisp
number_logopess (long x, lisp lx, long y, lisp ly,logope_code ope)
//#line 1010 "../src/num-arith.d"
{return fixnum_logope (x, y, ope);}

static inline lisp
number_logopesb (long x, lisp lx, const bignum_rep *y, lisp ly,logope_code ope)
//#line 1012 "../src/num-arith.d"
{
  bignum_rep *r = 0;
  bignum_rep_long xx (x);
  logope (r, ope, &xx, y);
  return make_integer (r);
}

static inline lisp
number_logopebs (const bignum_rep *x, lisp lx, long y, lisp ly,logope_code ope)
//#line 1021 "../src/num-arith.d"
{
  bignum_rep *r = 0;
  bignum_rep_long yy (y);
  logope (r, ope, x, &yy);
  return make_integer (r);
}

static inline lisp
number_logopebb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly,logope_code ope)
//#line 1028 "../src/num-arith.d"
{
  bignum_rep *r = 0;
  logope (r, ope, x, y);
  return make_integer (r);
}

//#line 1007 "../src/num-arith.d"
lisp
number_logope (lisp x, lisp y,logope_code ope)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_logopess (xshort_int_value (x), x, xshort_int_value (y), y,ope);
        case Tlong_int: return number_logopess (xshort_int_value (x), x, xlong_int_value (y), y,ope);
        case Tbignum: return number_logopesb (xshort_int_value (x), x, xbignum_rep (y), y,ope);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_logopess (xlong_int_value (x), x, xshort_int_value (y), y,ope);
        case Tlong_int: return number_logopess (xlong_int_value (x), x, xlong_int_value (y), y,ope);
        case Tbignum: return number_logopesb (xlong_int_value (x), x, xbignum_rep (y), y,ope);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return number_logopebs (xbignum_rep (x), x, xshort_int_value (y), y,ope);
        case Tlong_int: return number_logopebs (xbignum_rep (x), x, xlong_int_value (y), y,ope);
        case Tbignum: return number_logopebb (xbignum_rep (x), x, xbignum_rep (y), y,ope);
        }
    }
}

static inline lisp
Flognots (long x, lisp lx)
//#line 1038 "../src/num-arith.d"
{return make_fixnum (~x);}

static inline lisp
Flognotb (const bignum_rep *x, lisp lx)
//#line 1040 "../src/num-arith.d"
{
  bignum_rep *r = 0;
  lognot (r, x);
  return make_integer (r);
}

//#line 1035 "../src/num-arith.d"
lisp
Flognot (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP: return Flognots (xshort_int_value (x), x);
    case Tlong_int: return Flognots (xlong_int_value (x), x);
    case Tbignum: return Flognotb (xbignum_rep (x), x);
    }
}

static inline lisp
Flogtestss (long x, lisp lx, long y, lisp ly)
//#line 1050 "../src/num-arith.d"
{return boole (x & y);}

static inline lisp
Flogtestsb (long x, lisp lx, const bignum_rep *y, lisp ly)
//#line 1052 "../src/num-arith.d"
{
  bignum_rep_long xx (x);
  return boole (logtest (&xx, y));
}

static inline lisp
Flogtestbs (const bignum_rep *x, lisp lx, long y, lisp ly)
//#line 1059 "../src/num-arith.d"
{
  return Flogtestsb (y, ly, x, lx);
}

static inline lisp
Flogtestbb (const bignum_rep *x, lisp lx, const bignum_rep *y, lisp ly)
//#line 1061 "../src/num-arith.d"
{return boole (logtest (x, y));}

//#line 1047 "../src/num-arith.d"
lisp
Flogtest (lisp x, lisp y)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Flogtestss (xshort_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return Flogtestss (xshort_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return Flogtestsb (xshort_int_value (x), x, xbignum_rep (y), y);
        }
    case Tlong_int:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Flogtestss (xlong_int_value (x), x, xshort_int_value (y), y);
        case Tlong_int: return Flogtestss (xlong_int_value (x), x, xlong_int_value (y), y);
        case Tbignum: return Flogtestsb (xlong_int_value (x), x, xbignum_rep (y), y);
        }
    case Tbignum:
      switch (number_typeof (y))
        {
        default: FEtype_error (y, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Flogtestbs (xbignum_rep (x), x, xshort_int_value (y), y);
        case Tlong_int: return Flogtestbs (xbignum_rep (x), x, xlong_int_value (y), y);
        case Tbignum: return Flogtestbb (xbignum_rep (x), x, xbignum_rep (y), y);
        }
    }
}

static inline lisp
Flogcounts (long x, lisp lx)
//#line 1067 "../src/num-arith.d"
{
  int n = 0;
  for (u_long i = x >= 0 ? x : ~x; i; n++, i &= i - 1)
    ;
  return make_fixnum (n);
}

static inline lisp
Flogcountb (const bignum_rep *x, lisp lx)
//#line 1074 "../src/num-arith.d"
{return make_fixnum (logcount (x));}

//#line 1064 "../src/num-arith.d"
lisp
Flogcount (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP: return Flogcounts (xshort_int_value (x), x);
    case Tlong_int: return Flogcounts (xlong_int_value (x), x);
    case Tbignum: return Flogcountb (xbignum_rep (x), x);
    }
}

static inline lisp
Flogbitpss (long index, lisp lindex, long x, lisp lx)
//#line 1080 "../src/num-arith.d"
{
  if (index < 0)
    FErange_error (lindex);
  if (index >= BITS_PER_LONG)
    return boole (x < 0);
  return boole (x & (1 << index));
}

static inline lisp
Flogbitpsb (long index, lisp lindex, const bignum_rep *x, lisp lx)
//#line 1088 "../src/num-arith.d"
{
  if (index < 0)
    FErange_error (lindex);
  bignum_rep_long i (index);
  return boole (logbitp (x, index));
}

static inline lisp
Flogbitpbs (const bignum_rep *index, lisp lindex, long x, lisp lx)
//#line 1097 "../src/num-arith.d"
{
  if (index->minusp ())
    FErange_error (lindex);
  return boole (x < 0);
}

static inline lisp
Flogbitpbb (const bignum_rep *index, lisp lindex, const bignum_rep *x, lisp lx)
//#line 1103 "../src/num-arith.d"
{
  if (index->minusp ())
    FErange_error (lindex);
  return boole (x->minusp ());
}

//#line 1077 "../src/num-arith.d"
lisp
Flogbitp (lisp index, lisp x)
{
  switch (number_typeof (index))
    {
    default: FEtype_error (index, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (x))
        {
        default: FEtype_error (x, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Flogbitpss (xshort_int_value (index), index, xshort_int_value (x), x);
        case Tlong_int: return Flogbitpss (xshort_int_value (index), index, xlong_int_value (x), x);
        case Tbignum: return Flogbitpsb (xshort_int_value (index), index, xbignum_rep (x), x);
        }
    case Tlong_int:
      switch (number_typeof (x))
        {
        default: FEtype_error (x, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Flogbitpss (xlong_int_value (index), index, xshort_int_value (x), x);
        case Tlong_int: return Flogbitpss (xlong_int_value (index), index, xlong_int_value (x), x);
        case Tbignum: return Flogbitpsb (xlong_int_value (index), index, xbignum_rep (x), x);
        }
    case Tbignum:
      switch (number_typeof (x))
        {
        default: FEtype_error (x, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Flogbitpbs (xbignum_rep (index), index, xshort_int_value (x), x);
        case Tlong_int: return Flogbitpbs (xbignum_rep (index), index, xlong_int_value (x), x);
        case Tbignum: return Flogbitpbb (xbignum_rep (index), index, xbignum_rep (x), x);
        }
    }
}

static inline lisp
Fashbs (const bignum_rep *x, lisp lx, long count, lisp lcount)
//#line 1113 "../src/num-arith.d"
{
  bignum_rep *r = 0;
  ash (r, x, count);
  return make_integer (r);
}

static inline lisp
Fashbb (const bignum_rep *x, lisp lx, const bignum_rep *count, lisp lcount)
//#line 1119 "../src/num-arith.d"
{
  if (!count->minusp ())
    FEbignum_overflow ();
  return make_fixnum (x->minusp () ? -1 : 0);
}

static inline lisp
Fashss (long x, lisp lx, long count, lisp lcount)
//#line 1124 "../src/num-arith.d"
{
  if (count < 0)
    return make_fixnum (x >> -count);
  bignum_rep_long xx (x);
  return Fashbs (&xx, lx, count, lcount);
}

static inline lisp
Fashsb (long x, lisp lx, const bignum_rep *count, lisp lcount)
//#line 1131 "../src/num-arith.d"
{
  if (!count->minusp ())
    FEbignum_overflow ();
  return make_fixnum (x >= 0 ? 0 : -1);
}

//#line 1110 "../src/num-arith.d"
lisp
Fash (lisp x, lisp count)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP:
      switch (number_typeof (count))
        {
        default: FEtype_error (count, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Fashss (xshort_int_value (x), x, xshort_int_value (count), count);
        case Tlong_int: return Fashss (xshort_int_value (x), x, xlong_int_value (count), count);
        case Tbignum: return Fashsb (xshort_int_value (x), x, xbignum_rep (count), count);
        }
    case Tlong_int:
      switch (number_typeof (count))
        {
        default: FEtype_error (count, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Fashss (xlong_int_value (x), x, xshort_int_value (count), count);
        case Tlong_int: return Fashss (xlong_int_value (x), x, xlong_int_value (count), count);
        case Tbignum: return Fashsb (xlong_int_value (x), x, xbignum_rep (count), count);
        }
    case Tbignum:
      switch (number_typeof (count))
        {
        default: FEtype_error (count, Qinteger);
          /*NOTREACHED*/
        case Tshort_intP: return Fashbs (xbignum_rep (x), x, xshort_int_value (count), count);
        case Tlong_int: return Fashbs (xbignum_rep (x), x, xlong_int_value (count), count);
        case Tbignum: return Fashbb (xbignum_rep (x), x, xbignum_rep (count), count);
        }
    }
}

static inline lisp
Finteger_lengths (long x, lisp lx)
//#line 1144 "../src/num-arith.d"
{return make_fixnum (static_cast <long> (log2 (x >= 0 ? x : ~x)));}

static inline lisp
Finteger_lengthb (const bignum_rep *x, lisp lx)
//#line 1146 "../src/num-arith.d"
{return make_fixnum (x->howlong ());}

//#line 1141 "../src/num-arith.d"
lisp
Finteger_length (lisp x)
{
  switch (number_typeof (x))
    {
    default: FEtype_error (x, Qinteger);
      /*NOTREACHED*/
    case Tshort_intP: return Finteger_lengths (xshort_int_value (x), x);
    case Tlong_int: return Finteger_lengths (xlong_int_value (x), x);
    case Tbignum: return Finteger_lengthb (xbignum_rep (x), x);
    }
}

