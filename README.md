# Gnico版とは

-----
## このぷろじぇくとの目標的なもの
  * VisualStudio Code 使いやすくていいので，開発終了
  * ~~現代的なテキストエディタにする~~
    * ~~PythonやF#やHaskellなどの現代的な言語をサポート~~
    * ~~Windows 7等で追加された機能などを使えるようにする~~
  * ~~ダウンロードしてすぐに使えるエディタにする~~
-----
5ヶ月ほどコミットのない[x022235さんのリポジトリ](https://github.com/xyzzy-022/xyzzy)にある、xyzzyにいろいろな機能を追加したものです。(といってもあまりいじれてない)
py-mode, hs-mode, verilog-mode, f#-mode, Ispellが標準装備となっております。
また、Windows Vista以降のVisualStyleに対応しております。また、LispでGetVersionExをWindows 8.1 / 10 Technical Previewで偽のバージョンが返される対策もしました。
著作権ガン無視なので、訴えがあれば即削除します。でも、そこまで激おこぷんぷん丸にならないでねm(_ _)m

# 一応著作権情報
  * hs-mode
    * a simple haskell mode for xyzzy
    * [by T.Shido](http://www.geocities.jp/shido_takafumi/hs-mode.html)
  * fsharp-mode
    * [Information Page](http://www9.ocn.ne.jp/~y-kwsk/luvtechno/xyzzy.html)
  * verilog-mode
    * どこから持ってきたか忘れた
  * Ispell
    * やる気のないぺぇじから
  * py-mode
    * by Deecay氏, Furukawa Toru氏
  * paren改造版
    * (silog)[http://white.s151.xrea.com/wiki/index.php?script%2Fparen]より

# xyzzy

xyzzy は[亀井哲弥氏](http://www.jsdlab.co.jp/~kamei/) が開発した、Common Lisp っぽい言語(一般的にxyzzy lispと呼ばれる)
で拡張可能なEmacs っぽいテキストエディタのようなものです。
2ch とか Twitter とか五目並べとかハノイの塔とかができたり、テキストファイルの読み書きができます。

現在は亀井氏に変わり有志によって開発が継続しています。

----

## リリース

  * 不安定(といいますか、ほぼ自分専用なので…)

----

## ビルド方法

### 手順

 1. build.bat
    * デバッグ版は build.bat Debug
 2. bytecompile.bat
 3. ぽけーと待つ
 4. できあがり
 5. run-tests.bat でユニットテストを実行

----

## ライセンス

MIT ライセンスです。
詳細は LICENSE ファイルを参照のこと。
